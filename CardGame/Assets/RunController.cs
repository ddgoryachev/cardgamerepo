﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunController : MonoBehaviour
{
    [SerializeField] GameController gameController;
    [SerializeField] BattleController battleController;

    List<HeroInstance> heroes;
    List<CardInstance> deck;
    List<RelicInstance> relics;
    public AdventureInstance adventure;

    void Init(int heroID1, int heroID2)
    {
        InitHeroes(heroID1, heroID2);
        InitDeck();
        InitAdventure();

    }

    private void InitAdventure()
    {
        adventure = gameController.GetAdventureInstance(1);
    }

    private void InitDeck()
    {
        deck = new List<CardInstance>();
        heroes.ForEach(x => x.model.startingDeck.ForEach(
            y => deck.Add(gameController.GetCardInstance(y))
            ));
    }

    private void InitHeroes(int heroID1, int heroID2)
    {
        heroes = new List<HeroInstance>() {
            gameController.GetHeroInstance(heroID1),
            gameController.GetHeroInstance(heroID2)
        };
    }

    public void StartAdventure()
    {
        Init(1, 2);
        StartNextBattle();
    }

    public void StartNextBattle()
    {
        int nextRoomID = adventure.Next();
        BattleRoomInstance battleRoom = gameController.GetBattleRoomInstance(nextRoomID);

        battleController.Init(battleRoom, heroes, deck, relics);
    }
}
