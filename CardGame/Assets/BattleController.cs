﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;


public enum PHASE
{
    PLAYER,
    ENEMY
}

public class BattleController : MonoBehaviour
{
    public GameController gameController;
    public RunController runController;

    public HandController handController;
    public CardSelectionController cardSelectionController;
    public DeckContentsController deckContentsController;
    public BattleHudControler battleHudControler;
    public StatsController statsController;
    public BottomControlsController controlsController;
    public BattleRoomController battleRoomController;
    public BattleDeckController battleDeckController;
    public NextTurnController nextTurnController;

    public List<HeroInstance> heroes;
    public List<CardInstance> deck;
    public List<RelicInstance> relics;

    public TextMeshProUGUI battleInfoString;

    PHASE phase;
    int turn;
    public bool win;

    public PHASE Phase => phase;
    public int Turn => turn;

    internal void Init(BattleRoomInstance room, List<HeroInstance> hrs, List<CardInstance> dck, List<RelicInstance> rlcs)
    {
        win = false;
        API.Monster.battleController = this;
        API.Card.battleController = this;
        API.Func.battleController = this;
        API.NoAPI.battleController = this;

        gameController = FindObjectOfType<GameController>();
        heroes = hrs;
        deck = dck;
        relics = rlcs;

        turn = 1;

        statsController.Init(heroes, room);
        controlsController.Init(deck);
        battleDeckController.Init();

        battleInfoString.text = runController.adventure.GetInfoString();
    }

    public void EndTurn()
    {
        Debug.Log("end_turn" + Turn);

        nextTurnController.gameObject.SetActive(false);

        if (gameController.gameConstants.AUTO_DISCARD)
        {
            battleDeckController.AutoDiscard();
            MakeMonsterTurn();
        }
        else
        {
            throw new NotImplementedException();
        }
    }

    internal void DealDamageToRandomMonsterMultiple(int damage, int mult, bool piercing, CharController hero)
    {
        statsController.monstersController.DealDamageToRandomMonsterMultiple(damage, mult, piercing, hero);
    }

    private void MakeMonsterTurn()
    {
        phase = PHASE.ENEMY;
        statsController.DoMonstersTurn();
        StartTurn();
    }

    private void StartTurn()
    {
        turn++;
        statsController.StatusCountdown();
        battleDeckController.StartTurn();
        nextTurnController.gameObject.SetActive(true);
    }

    public CharController GetCharByIID(int iID)
    {
        CharController result = statsController.monstersController.GetMonsterByIID(iID);
        return result ? result : statsController.heroesController.GetHeroByIID(iID);
    }

    public void FinishBattle()
    {
        // вернуть героев на карту с обновлёнными статами
    }

    internal void Win()
    {
        win = true;
    }

    internal CardController GetCardByIID(int cardIID)
    {
        return battleDeckController.GetCardByIID(cardIID);
    }

    internal StatusInstance GetStatusInstanceByIID(int iID)
    {
        return statsController.GetStatusInstanceByIID(iID);
    }

    internal void CheckWinCardPlayed()
    {
        if (win)
        {
            battleDeckController.ClearHand();
            runController.StartNextBattle();
        }
    }
}
