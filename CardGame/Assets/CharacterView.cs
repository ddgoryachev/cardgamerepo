﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterView : MonoBehaviour
{
    public Color normalColor;
    public int normalOrder;
    public Color focusedColor;
    public int focusedOrder;
    public SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        normalColor = spriteRenderer.color;
        normalOrder = spriteRenderer.sortingOrder;
    }

    public void Show()
    {
        spriteRenderer.sortingOrder = focusedOrder;
    }

    public void SetFocus(bool focus)
    {
        spriteRenderer.color = focus ? focusedColor : normalColor;
    }

    public void Hide()
    {
        spriteRenderer.sortingOrder = normalOrder;
        spriteRenderer.color = normalColor;
    }
}

