﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class CharView : MonoBehaviour
{
    [SerializeField] HealthBarView hpBar;
    [SerializeField] CharController charController;
    [SerializeField] IntentionView intentionView;
    [SerializeField] SpriteRenderer charSprite;
    [SerializeField] TextMeshProUGUI iID;
    [SerializeField] StatusBarController statusBarController;

    public void UpdateAll()
    {
        UpdateStats();
        UpdateView();
        UpdateStatuses();
    }

    public void UpdateStats()
    {
        if (charController.dead)
        {
            hpBar.gameObject.SetActive(false);
        }
        else if (charController.hero != null || charController.monster != null)
        {
            hpBar.gameObject.SetActive(true);
            hpBar.UpdateView();
        }
        else
        {
            hpBar.gameObject.SetActive(false);
        }
    }

    internal void UpdateView()
    {
        if (charController.dead)
        {
            intentionView.gameObject.SetActive(false);
            charSprite.sprite = null;
            iID.text = "";
        }
        else if (charController.IsMonster())
        {
            intentionView.gameObject.SetActive(true);
            intentionView.UpdateView();
            charSprite.sprite = Resources.Load<Sprite>(charController.monster.model.art);
            iID.text = "IID: " + charController.monster.instanceID;
        }
        else if (charController.hero != null)
        {
            charSprite.sprite = Resources.Load<Sprite>(charController.hero.model.art);
            iID.text = "IID: " + charController.hero.instanceID;
        }
        else
        {
            intentionView.gameObject.SetActive(false);
            charSprite.sprite = null;
            iID.text = "";
        }
    }

    public void UpdateStatuses()
    {
        statusBarController.UpdateAll();
    }
}
