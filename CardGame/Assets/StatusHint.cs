﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;

public class StatusHint : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] TextMeshProUGUI text;

    public void Show(string t)
    {
        image.enabled = true;
        text.text = t;
    }

    public void Hide()
    {
        image.enabled = false;
        text.text = "";
    }

}
