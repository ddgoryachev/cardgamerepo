﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleDeckController : MonoBehaviour
{
    GameController gameController;
    public BattleController battleController;
    BottomControlsController controlsController;
    [SerializeField] HandController handController;

    int drawCount;
    int handSize;
    public int maxActions;
    public int actions;


    public List<CardInstance> drawPile;
    public List<CardInstance> discardPile;
    public List<CardInstance> exilePile;
    public List<CardInstance> hand;

    private void Start()
    {
        battleController = FindObjectOfType<BattleController>();
        gameController = FindObjectOfType<GameController>();
        controlsController = FindObjectOfType<BottomControlsController>();
    }

    internal bool drag;

    public void Init()
    {
        discardPile = new List<CardInstance>();
        exilePile = new List<CardInstance>();
        hand = new List<CardInstance>();

        InitSizes();
        InitDrawPile();
        InitDiscardPile();
        InitExilePile();
        InitHand();
        DrawCards(drawCount);
    }

    private void InitSizes()
    {
        drawCount = gameController.player.attributes[HERO_ATTRIBUTE.DRAW];
        handSize = gameController.player.attributes[HERO_ATTRIBUTE.HAND];
        actions = gameController.player.attributes[HERO_ATTRIBUTE.ACTIONS];
        

        foreach (HeroInstance hero in battleController.heroes)
        {
            drawCount += hero.attributes[HERO_ATTRIBUTE.DRAW];
            handSize += hero.attributes[HERO_ATTRIBUTE.HAND];
            actions += hero.attributes[HERO_ATTRIBUTE.ACTIONS];
        }
        maxActions = actions;
        controlsController.actionView.UpdateView();
    }

    private void InitDrawPile()
    {
        List<CardInstance> result = new List<CardInstance>();
        foreach (CardInstance card in battleController.deck)
        {
            result.Insert(gameController.rnd.Next(result.Count), card);
        }
        drawPile = result;
        UpdateDrawPileView();
    }

    internal void StartTurn()
    {
        actions = maxActions;
        UpdateDrawPileView();
        controlsController.actionView.UpdateView();
        DrawCards(drawCount);
    }

    internal void AutoDiscard()
    {
        while (hand.Count > 0)
        {
            DiscardOrExile(hand[hand.Count - 1], false, true);
        }
    }

    public void AddAction(int action)
    {
        actions += action;
        controlsController.actionView.UpdateView();
    }

    public void PayAction(int cost)
    {
        actions -= cost;
        controlsController.actionView.UpdateView();
    }

    private void InitDiscardPile()
    {
        discardPile = new List<CardInstance>();
        UpdateDiscardPileView();
    }

    private void InitExilePile()
    {
        exilePile = new List<CardInstance>();
        UpdateExilePileView();
    }

    private void UpdateExilePileView()
    {
        controlsController.exilePile.ViewUpdate();
    }

    private void InitHand()
    {
        handController.Init();
        
    }

    public void DrawCards(int count)
    {
        StartCoroutine(DrawDelayed(count));
    }

    public bool DrawCard()
    {
        if (drawPile.Count == 0)
        {
            RefillDrawPile();
        }
        if (drawPile.Count == 0) return false;

        CardInstance cardToDraw = drawPile[drawPile.Count - 1];
        drawPile = drawPile.GetRange(0, drawPile.Count - 1);
        handController.DrawCard(cardToDraw);
        hand.Add(cardToDraw);
        UpdateDrawPileView();
        return true;
    }

    private void RefillDrawPile()
    {
        List<CardInstance> result = new List<CardInstance>();
        foreach (CardInstance card in discardPile)
        {
            result.Insert(gameController.rnd.Next(result.Count), card);
        }
        drawPile = result;
        discardPile = new List<CardInstance>();
        UpdateDiscardPileView();
        UpdateDrawPileView();
    }

    private void UpdateDrawPileView()
    {
        controlsController.drawPile.ViewUpdate();
    }

    private void UpdateDiscardPileView()
    {
        controlsController.discardPile.ViewUpdate();
    }

    IEnumerator DrawDelayed(int count)
    {
        for (int i = 0; i < count; i++)
        {
            if (!DrawCard() || i == count - 1) break;
            yield return new WaitForSeconds(0.2f);
        }
    }

    internal void DiscardOrExile(CardInstance card, bool exile, bool played)
    {
        if (!exile) discardPile.Add(card);
        else exilePile.Add(card);

        UpdateDiscardPileView();
        UpdateExilePileView();

        handController.DiscardCard(card, played);
        hand = hand.FindAll(x => x != card);
    }

    public CardController GetCardByIID(int IID)
    {
        return handController.cardControllers.Find(x => x.cardInstance.instanceID == IID);
    }
    
    public void RefreshHand()
    {
        AutoDiscard();
        DrawCards(drawCount);
        actions = maxActions;
        controlsController.actionView.UpdateView();
    }

    public void ClearHand()
    {
        handController.Clear();
    }
}
