﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonstersController : MonoBehaviour
{
    [SerializeField] BattleController battleController;
    [SerializeField] public List<CharController> monsterControllers;
    [SerializeField] GameController gameController;

    internal void Init(List<int> monsterIDs)
    {

        List<MonsterInstance> monsters = new List<MonsterInstance>();
        monsterIDs.ForEach(x => monsters.Add(gameController.GetMonsterInstance(x)));

        UpdateControllers(monsters);

    }

    internal void Init(List<MonsterInstance> monsters)
    {
        UpdateControllers(monsters);

    }

    private void UpdateControllers(List<MonsterInstance> monsters)
    {
        for (int i = 0; i < monsterControllers.Count; i++)
        {
            if (i < monsters.Count) {
                if (monsters[i] != null) monsterControllers[i].Init(monsters[i]);
                else { monsterControllers[i].Init(); }
            }
            else
            {
                monsterControllers[i].Init();
            }
        }
    }

    internal void DoTurn()
    {
        monsterControllers.ForEach(x => { if (!x.dead && !x.IsEmpty()) x.DoAction(); });

    }

    public CharController GetMonsterByIID(int iID)
    {
        return monsterControllers.Find(x => !x.IsEmpty() && !x.dead && x.monster.instanceID == iID);
    }

    public int GetMonsterPosition(CharController monster)
    {
        return monsterControllers.FindIndex(x => !x.IsEmpty() && !x.dead && x == monster);
    } 

    internal int GetClosest()
    {
        return monsterControllers.FindIndex(x => !x.dead && !x.IsEmpty());
    }

    public void PushMonster(CharController target)
    {
        if (!target || target.IsEmpty() || target.dead) return;
        int index = GetMonsterPosition(target);
        if (index == 2) return;
        int index2 = index + 1;

        Swap(index, index2);
    }

    public void PullMonster(CharController target)
    {
        if (!target || target.IsEmpty() || target.dead) return;
        int index = GetMonsterPosition(target);
        if (index == 0) return;
        int index2 = index - 1;

        Swap(index, index2);
    }

    internal void DealDamageToRandomMonsterMultiple(int damage, int mult, bool piercing, CharController hero)
    {
        for (int i = 0; i < mult; i++)
        {
            List<CharController> monsters = monsterControllers.FindAll(x => !x.dead && !x.IsEmpty());
            if (monsters.Count == 0) return;
            int index = UnityEngine.Random.Range(0, monsters.Count);
            CharController target = monsters[index];

            damage = API.NoAPI.CalcCardDamage(damage, hero.hero, target.monster);
            target.ReceiveDamageMultiple(damage, 1, piercing, hero);
        }
    }

    internal void CheckWin(CharController charController)
    {
        if (monsterControllers.TrueForAll(x => x.dead || x.IsEmpty())) battleController.Win();
    }

    private void Swap(int i0, int i1)
    {
        List<MonsterInstance> monsters = new List<MonsterInstance>();
        for (int i = 0; i < monsterControllers.Count; i++)
        {
            if (i == i0) monsters.Add(monsterControllers[i1].monster);
            else if (i == i1) monsters.Add(monsterControllers[i0].monster);
            else monsters.Add(monsterControllers[i].monster);
        }

        Init(monsters);
    }

    public CharController GetMonsterByPosition(int pos)
    {
        if (pos < 0 || pos > 2 
            || monsterControllers[pos].dead 
            || monsterControllers[pos].IsEmpty()) return null;
        else return monsterControllers[pos];
    }

    internal StatusInstance GetStatusInstanceByIID(int iID)
    {
        foreach (CharController charController in monsterControllers)
        {
            StatusInstance st = charController.GetStatusByIID(iID);
            if (st != null) return st;
        }
        return null;
    }
}
