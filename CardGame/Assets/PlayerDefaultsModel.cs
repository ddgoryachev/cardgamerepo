﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MoonSharp.Interpreter;

public class PlayerDefaultsModel
{
    public readonly Dictionary<HERO_ATTRIBUTE, int> attributes;

    public PlayerDefaultsModel(Table table)
    {
        attributes = GetAttributes(table);
    }

    private Dictionary<HERO_ATTRIBUTE, int> GetAttributes(Table table)
    {
        Dictionary<HERO_ATTRIBUTE, int> result = new Dictionary<HERO_ATTRIBUTE, int>();
        Table rawAttributes = table.Get("attributes").Table;
        foreach (DynValue attribute in rawAttributes.Keys)
        {
            // is it OK?
            result.Add((HERO_ATTRIBUTE)attribute.Number, (int)rawAttributes.Get(attribute).Number);
        }
        return result;
    }
}