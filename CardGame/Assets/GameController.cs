﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public System.Random rnd;
    public GameConstants gameConstants;
    public PlayerDefaultsModel player;
    Dictionary<int, CardModel> cardModelsMap;
    Dictionary<int, StatusModel> statusModelsMap;
    Dictionary<int, HeroModel> heroModelsMap;
    Dictionary<int, MonsterModel> monsterModelsMap;
    Dictionary<int, BattleRoomModel> battleRoomsMap;
    Dictionary<int, AdventureModel> adventuresMap;
    public Dictionary<MONSTER_INTENTION, string> intentionIcons;


    private void Awake()
    {
        Init();
    }

    public void Init()
    {
        InitConfigs();
    }

    void InitConfigs()
    {
        rnd = new System.Random();
        LuaHelper.InitScript();
        API.InstanceID.Init();
        gameConstants = LuaHelper.GetGameConstants();
        player = LuaHelper.GetPlayerDefaults();
        cardModelsMap = LuaHelper.GetCards();
        statusModelsMap = LuaHelper.GetStatuses();
        heroModelsMap = LuaHelper.GetHeroes();
        monsterModelsMap = LuaHelper.GetMonsters();
        intentionIcons = LuaHelper.GetIntentionIcons();
        battleRoomsMap = LuaHelper.GetBattleRoomModels();
        adventuresMap = LuaHelper.GetAdventureModels();
    }


    public HeroInstance GetHeroInstance(int id)
    {
        return new HeroInstance(heroModelsMap[id], this);
    }

    public MonsterInstance GetMonsterInstance(int id)
    {
        if (id == 0) return null;
        return new MonsterInstance(monsterModelsMap[id], this);
    }

    public CardInstance GetCardInstance(int id)
    {

        return new CardInstance(cardModelsMap[id]);
    }

    public BattleRoomInstance GetBattleRoomInstance(int id)
    {
        return new BattleRoomInstance(battleRoomsMap[id]);
    }

    public StatusInstance GetStatusInstance(int id, int counter)
    {
        return new StatusInstance(statusModelsMap[id], counter);
    }

    public string GetHeroNameById(int id)
    {
        return heroModelsMap[id].name;
    }

    public AdventureInstance GetAdventureInstance(int id)
    {
        return new AdventureInstance(adventuresMap[id]);
    }
}
