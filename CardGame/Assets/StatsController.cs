﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsController : MonoBehaviour
{
    public HeroesController heroesController;
    public MonstersController monstersController;
    public GameController gameController;

    internal void Init(List<HeroInstance> heroes, BattleRoomInstance room)
    {
        gameController = FindObjectOfType<GameController>();
        heroesController.Init(heroes);
        monstersController.Init(room.model.monsters);
    }

    internal void DoMonstersTurn()
    {
        monstersController.DoTurn();
    }

    internal void StatusCountdown()
    {
        heroesController.charControllers.ForEach(x => x.StatusCountdown());
        monstersController.monsterControllers.ForEach(x => x.StatusCountdown());
    }

    internal StatusInstance GetStatusInstanceByIID(int iID)
    {
        StatusInstance result = heroesController.GetStatusInstanceByIID(iID);
        if (result != null) return result;
        result = monstersController.GetStatusInstanceByIID(iID);
        if (result != null) return result;
        else return null;
    }
}
