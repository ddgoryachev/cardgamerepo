﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour
{
    public List<CardController> cardControllers;
    [SerializeField] GameObject cardViewPrefab;
    [SerializeField] RectTransform rectTransform;
    float cardWidth;
    float handMinX;
    float handMaxX;
    public float maxCardDistance;

    private void Start()
    {
        cardWidth = cardViewPrefab.GetComponent<RectTransform>().sizeDelta.x;
        handMaxX = GetComponent<RectTransform>().sizeDelta.x / 2 - cardWidth/2;
        handMinX = -handMaxX;
    }

    public void Init()
    {
        cardControllers = new List<CardController>();
    }

    internal void DrawCard(CardInstance cardToDraw)
    {
        GameObject newCard = Instantiate(cardViewPrefab, transform);
        CardController cardController = newCard.GetComponent<CardController>();
        cardController.Init(cardToDraw, CARD_BEHAVIOR.HAND);
        cardControllers.Add(cardController);
        cardController.DoAction(CARD_ACTION.ON_DRAW, 0);
        DistributeHandCards();
    }

    internal void DoActions(CARD_ACTION act, CharController p)
    {
        cardControllers.ForEach(x => x.DoAction(act, p ? p.GetIID() : 0));
    }

    public void DiscardCard(CardInstance card, bool played)
    {
        CardController discarded = cardControllers.Find(x => x.cardInstance == card);
        cardControllers = cardControllers.FindAll(x => x.cardInstance != card);

        if (!played) discarded.DoAction(CARD_ACTION.ON_DISCARD, 0);

        discarded.Destroy();
        DistributeHandCards();
    }

    public void DistributeHandCards()
    {
        float maxWidth = handMaxX - handMinX;
        float sparseWidth = (cardControllers.Count - 1) * maxCardDistance;
        float middle = (handMaxX + handMinX) / 2f;

        if (sparseWidth < maxWidth)
        {
            float start = middle - sparseWidth / 2f;
            for (int i = 0; i < cardControllers.Count; i++)
            {
                RectTransform rect = cardControllers[i].GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(start + maxCardDistance * i, 0);
                cardControllers[i].SetOrder(i);
                
            }
        }
        else
        {
            float start = handMinX;
            for (int i = 0; i < cardControllers.Count; i++)
            {
                RectTransform rect = cardControllers[i].GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(start + maxWidth / (cardControllers.Count - 1) * i, 0);
                cardControllers[i].SetOrder(i);
            }
        }
    }

    internal void Clear()
    {
        cardControllers.ForEach(x => x.Destroy());
        cardControllers = new List<CardController>();
    }
}
