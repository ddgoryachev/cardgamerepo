﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StatusPopupView : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    StatusInstance status;

    internal void Show(StatusInstance instance)
    {
        status = instance;
        string header = "<u><b>" + instance.model.alias + "</u></b>" + System.Environment.NewLine;
        string desc = MakeString(instance.model.description);
        text.text = header + desc;
    }

    private string MakeString(string str)
    {
        foreach (STATUS_ATTRIBUTE attr in status.attributes.Keys)
        {
            int value = GetAttribute(attr);
            switch (attr)
            {
                
            }
            string tag = "<color=#FFFFFF>";

            str = str.Replace("{" + (int)attr + "}", "<b>" + tag + value.ToString() + "</color></b>");
        }
        return str;
    }

    private int GetAttribute(STATUS_ATTRIBUTE attr)
    {
        return status.attributes.ContainsKey(attr) ?
            status.attributes[attr] : 0;
    }
}
