﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IntentionView : MonoBehaviour
{
    CharController charController;
    GameController gameController;
    [SerializeField] Image icon;
    [SerializeField] RectTransform spacing;
    [SerializeField] TextMeshProUGUI dmgText;
    [SerializeField] RectTransform rectTransform;


    void Start()
    {
        charController = GetComponentInParent<CharController>();
        gameController = FindObjectOfType<GameController>();
    }

    public void UpdateView()
    {
        if (charController.IsMonster())
        {
            MonsterInstance monster = charController.monster;
            MonsterAction action = monster.GetPreparedAction();
            var attr = action.attributes;

            switch (action.intention)
            {
                case MONSTER_INTENTION.MELEE:
                case MONSTER_INTENTION.RANGED:
                    string addText = attr.ContainsKey(ACTION_ATTRIBUTE.MULTIPLIER) 
                        ? "x" + attr[ACTION_ATTRIBUTE.MULTIPLIER].ToString() : "";
                    dmgText.text = attr[ACTION_ATTRIBUTE.DAMAGE].ToString() + addText;
                    spacing.gameObject.SetActive(true);
                    icon.sprite = Resources.Load<Sprite>(gameController.intentionIcons[action.intention]);
                    break;
                case MONSTER_INTENTION.DEFEND_SELF:
                    dmgText.text = "";
                    spacing.gameObject.SetActive(false);
                    icon.sprite = Resources.Load<Sprite>(gameController.intentionIcons[action.intention]);
                    break;
                case MONSTER_INTENTION.WAIT:
                    dmgText.text = "";
                    spacing.gameObject.SetActive(false);
                    icon.sprite = Resources.Load<Sprite>(gameController.intentionIcons[action.intention]);
                    break;
                case MONSTER_INTENTION.UNKNOWN:
                    dmgText.text = "";
                    spacing.gameObject.SetActive(false);
                    icon.sprite = null;
                    break;
            }

            rectTransform.anchoredPosition = new Vector2Int(0, monster.model.intentionHeight);
        }
        else
        {
            gameObject.SetActive(false);
        }
        
    }
}
