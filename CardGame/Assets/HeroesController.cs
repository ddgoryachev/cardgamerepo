﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroesController : MonoBehaviour
{
    [SerializeField] BattleRoomController roomController;
    public BattleController battleController;

    public List<CharController> charControllers;

    internal void Init(List<HeroInstance> heroes)
    {
        for (int i = 0; i<2; i++)
        {
            charControllers[i].Init(heroes[i]);
        }
        battleController = FindObjectOfType<BattleController>();
    }

    [ContextMenu("SWAP")]
    public void Swap()
    {
        HeroInstance temp0 = charControllers[0].hero;
        HeroInstance temp1 = charControllers[1].hero;

        List<HeroInstance> heroes = new List<HeroInstance>() {
            charControllers[1].hero,
            charControllers[0].hero
        };

        Init(heroes);

        battleController.handController.DoActions(CARD_ACTION.ON_HEROES_SWAP, null);
    }

    internal StatusInstance GetStatusInstanceByIID(int iID)
    {
        foreach (CharController charController in charControllers)
        {
            StatusInstance st = charController.GetStatusByIID(iID);
            if (st != null) return st;
        }
        return null;
    }

    internal CharController GetHeroByIID(int iID)
    {
        return charControllers.Find(x => !x.IsEmpty() && !x.dead && x.hero.instanceID == iID);
    }

    public int GetHeroPosition(CharController hero)
    {
        return charControllers.FindIndex(x => !x.IsEmpty() && !x.dead && x == hero);
    }
}
