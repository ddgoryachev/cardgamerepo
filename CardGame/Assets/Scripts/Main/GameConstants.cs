﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;

public class GameConstants
{
    public readonly bool AUTO_DISCARD;
    public readonly bool PERSISTENT_ARMOR;

    public GameConstants(Table table)
    {
        AUTO_DISCARD = table.Get("AUTO_DISCARD").Boolean;
        PERSISTENT_ARMOR = table.Get("PERSISTENT_ARMOR").Boolean;

    }
}