﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MoonSharp.Interpreter;


public abstract class BaseInstance
{
    public abstract int GetModelID();
}


namespace API
{
    public static class InstanceID
    {
        static int instanceID = 0;
        public static int Next(object instance)
        {
            instanceID++;
            return instanceID;
        }
        public static void Init() {
            instanceID = 0;
        }
        
    }

    public static class Monster
    {
        public static BattleController battleController;
        public static Script script;

        public static void AttackByAttributes(int iID)
        {
            CharController monster = battleController.statsController.monstersController.GetMonsterByIID(iID);
            if (monster) monster.AttackByAttributes();
        }

        public static void DefendByAttributes(int iID)
        {
            CharController monster = battleController.statsController.monstersController.GetMonsterByIID(iID);
            if (monster) monster.DefendByAttributes();
        }
    }

    public static class Card
    {
        public static Script script;
        public static BattleController battleController;

        public static void AttackByAttributes(int cardIID, int targetIID, CARD_ATTRIBUTE attrDMG, CARD_ATTRIBUTE attrMULT, bool piercing)
        {
            CardController card = battleController.GetCardByIID(cardIID);
            int dmg = card.cardInstance.attributes[attrDMG];
            int mult = card.cardInstance.GetAttribute(attrMULT);
            CharController hero = battleController.GetCharByIID(API.Func.GetHeroByCardRange(cardIID));

            if (card.cardInstance.model.cardTarget == CARD_TARGET.RANDOM)
            {
                battleController.DealDamageToRandomMonsterMultiple(dmg, mult, piercing, hero);
            }
            else
            {
                CharController target = battleController.GetCharByIID(targetIID);
                dmg = API.NoAPI.CalcCardDamage(dmg, hero.hero, target.monster);
                target.ReceiveDamageMultiple(dmg, mult, piercing, hero);
            }
        }

        public static void DefendByAttributes(int cardIID, int targetIID, CARD_ATTRIBUTE attrARMR)
        {
            CharController target = battleController.GetCharByIID(targetIID);
            CardController card = battleController.GetCardByIID(cardIID);
            int armr = card.cardInstance.GetAttribute(attrARMR);
            target.AddArmor(armr, true);
        }

        public static void SwapHeroes()
        {
            battleController.statsController.heroesController.Swap();
        }

        public static void PushMonster(int targetIID)
        {
            CharController target = battleController.GetCharByIID(targetIID);
            battleController.statsController.monstersController.PushMonster(target);
        }

        public static void PullMonster(int targetIID)
        {
            CharController target = battleController.GetCharByIID(targetIID);
            battleController.statsController.monstersController.PullMonster(target);
        }

        public static void DrawCardsByCounter(int cardIID, CARD_ATTRIBUTE attrCNTR)
        {
            CardController card = battleController.GetCardByIID(cardIID);
            int counter = card.cardInstance.GetAttribute(attrCNTR);
            battleController.battleDeckController.DrawCards(counter);
        }

        public static void AttackExplicit(int cardIID, int targetIID, int dmg, int mult, bool piercing)
        {
            CardController card = battleController.GetCardByIID(cardIID);
            int range = card.cardInstance.model.range;
            CharController hero = battleController.statsController.heroesController.charControllers[range];

            if (card.cardInstance.model.cardTarget == CARD_TARGET.RANDOM)
            {
                battleController.DealDamageToRandomMonsterMultiple(dmg, mult, piercing, hero);
            }
            else
            {
                CharController target = battleController.GetCharByIID(targetIID);
                dmg = API.NoAPI.CalcCardDamage(dmg, hero.hero, target.monster);
                target.ReceiveDamageMultiple(dmg, mult, piercing, hero);
            }
        }

        public static void DiscardPlayed(int cardIID)
        {
            CardController card = battleController.battleDeckController.GetCardByIID(cardIID);
            battleController.battleDeckController.DiscardOrExile(card.cardInstance, false, true);
        }

        internal static void SetAttribute(int cardIID, CARD_ATTRIBUTE attr, int value)
        {
            CardController card = battleController.battleDeckController.GetCardByIID(cardIID);
            card.SetAttribute(attr, value);
        }

        internal static void ActionLink(int cardIID, int target, CARD_ACTION act)
        {
            CardController card = battleController.battleDeckController.GetCardByIID(cardIID);
            card.DoAction(act, target);
        }

        internal static void DefendExplicit(int cardIID, int targetIID, int armor)
        {
            CharController target = battleController.GetCharByIID(targetIID);
            target.AddArmor(armor, true);
        }

        internal static void AddStatusByAttributes(int cardIID, int targetIID, int statusID, CARD_ATTRIBUTE attr)
        {
            CardController card = battleController.battleDeckController.GetCardByIID(cardIID);
            CharController target = battleController.GetCharByIID(targetIID);
            int counter = card.cardInstance.GetAttribute(attr);
            target.AddStatus(statusID, counter);
        }

        internal static void SetMonsterAttribute(int monsterIID, MONSTER_ATTRIBUTE attr, int value)
        {
            CharController monster = battleController.GetCharByIID(monsterIID);
            monster.SetAttribute(attr, value);
        }

    }

    public static class Func
    {
        public static Script script;
        public static BattleController battleController;

        internal static bool AssertClass(int cardIID)
        {
            CardController card = battleController.battleDeckController.GetCardByIID(cardIID);
            int cardHeroID = card.cardInstance.model.heroID;
            int range = card.cardInstance.model.range;
            int playingHeroID = battleController.statsController.heroesController.charControllers[range].hero.model.id;
            return cardHeroID == playingHeroID; 
        }

        internal static int GetHeroByCardRange(int cardIID)
        {
            CardController card = battleController.battleDeckController.GetCardByIID(cardIID);
            int range = card.cardInstance.model.range;
            CharController hero = battleController.statsController.heroesController.charControllers[range];
            return hero.hero.instanceID;
        }

        internal static int GetHeroAttribute(int heroIID, HERO_ATTRIBUTE attr)
        {
            CharController hero = battleController.GetCharByIID(heroIID);

            return hero.hero.GetAttribute(attr);
        }

        internal static int GetCardModelAttribute(int cardIID, CARD_ATTRIBUTE attr)
        {
            CardController card = battleController.battleDeckController.GetCardByIID(cardIID);
            return card.cardInstance.GetModelAttribute(attr);
        }

        internal static int GetMonsterPositionByIID(int targetIID)
        {
            CharController target = battleController.GetCharByIID(targetIID);
            return battleController.statsController.monstersController.GetMonsterPosition(target);
        }

        internal static int GetHeroPositionByIID(int heroIID)
        {
            CharController target = battleController.GetCharByIID(heroIID);
            return battleController.statsController.heroesController.GetHeroPosition(target);
        }

        internal static int GetMonsterIIDByPosition(int position)
        {
            CharController target = battleController.statsController.monstersController.GetMonsterByPosition(position);
            return target ? target.monster.instanceID : 0;
        }

        internal static int GetMonsterAttribute(int monsterIID, MONSTER_ATTRIBUTE attr)
        {
            CharController monster = battleController.GetCharByIID(monsterIID);

            return monster.monster.GetAttribute(attr);
        }

        internal static int GetHeroByRange(int range)
        {
            CharController hero = battleController.statsController.heroesController.charControllers[range];
            return hero.hero.instanceID;
        }

        internal static int GetModelIDByIID(int iID)
        {
            CharController character = battleController.GetCharByIID(iID);
            if (character) return character.GetModelID();

            StatusInstance status = battleController.GetStatusInstanceByIID(iID);
            if (status != null) return status.GetModelID();

            CardController card = battleController.GetCardByIID(iID);
            if (card != null) return card.GetModelID();

            return -1;
        }
    }

    public static class NoAPI
    {
        public static BattleController battleController;

        public static int CalcCardDamage(int baseDmg, HeroInstance hero, MonsterInstance monster)
        {
            int heroPos = API.Func.GetHeroPositionByIID(hero.instanceID);
            int monsterPos = API.Func.GetMonsterPositionByIID(monster.instanceID);

            float damage = baseDmg + hero.GetStatusAttributesSumm(heroPos == 0 ? STATUS_ATTRIBUTE.M_DAMAGE : STATUS_ATTRIBUTE.R_DAMAGE);
            damage = damage * hero.GetStatusModifiersProduct(heroPos == 0 ? STATUS_MODIFIER.M_DAMAGE : STATUS_MODIFIER.R_DAMAGE);
            damage = damage * monster.GetStatusModifiersProduct(monsterPos == 0 ? STATUS_MODIFIER.M_INCOMING : STATUS_MODIFIER.R_INCOMING);
            damage = damage + monster.GetStatusAttributesSumm(monsterPos == 0 ? STATUS_ATTRIBUTE.M_DAMAGE : STATUS_ATTRIBUTE.R_DAMAGE);

            return Mathf.RoundToInt(damage);
        }

        public static int CalcMonsterDamage(int baseDmg, int heroPos, int monsterPos, HeroInstance hero, MonsterInstance monster)
        {
            float damage = baseDmg + monster.GetStatusAttributesSumm(monsterPos == 0 ? STATUS_ATTRIBUTE.M_DAMAGE : STATUS_ATTRIBUTE.R_DAMAGE);
            damage = damage * monster.GetStatusModifiersProduct(monsterPos == 0 ? STATUS_MODIFIER.M_DAMAGE : STATUS_MODIFIER.R_DAMAGE);
            damage = damage * hero.GetStatusModifiersProduct(heroPos == 0 ? STATUS_MODIFIER.M_INCOMING : STATUS_MODIFIER.R_INCOMING);
            damage = damage + hero.GetStatusAttributesSumm(heroPos == 0 ? STATUS_ATTRIBUTE.M_DAMAGE : STATUS_ATTRIBUTE.R_DAMAGE);

            return Mathf.RoundToInt(damage);
        }

        public static float CalcHitChance(CharController attacker, CharController target)
        {
            STATUS_MODIFIER attMod = GetCharPosition(attacker) > 0
                ? STATUS_MODIFIER.R_ACCURACY 
                : STATUS_MODIFIER.M_ACCURACY;

            float chanceToHit = attacker.GetStatusModifiersProduct(attMod);
            float chanceToFailEvasion = target.GetFullEvasionFail(GetCharPosition(target));

            return chanceToHit * chanceToFailEvasion;
        }

        public static int GetCharPosition(CharController charController)
        {
            return charController.IsMonster()
                ? battleController.statsController.monstersController.GetMonsterPosition(charController)
                : battleController.statsController.heroesController.GetHeroPosition(charController);
        } 
    }
}
