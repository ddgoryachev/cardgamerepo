﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

using MoonSharp.Interpreter;


public static class LuaHelper
{
    public static Script script = new Script();
    private static Table _API;
    private static Table _GameAction;
    private static Table _GameFunction;

    private static Table _Monster;
    private static Table _Card;
    private static Table _Func;

    public static void InitScript()
    {
        Script.DefaultOptions.DebugPrint = s => Debug.Log(s.ToLower());
        script = new Script();
        TextAsset textAsset = Resources.Load<TextAsset>("config/configs");
        string scriptText = textAsset.text;
        script.DoString(scriptText);

        DeclareAPI();
    }

    private static void DeclareAPI()
    {
        _API = script.Globals.Get("API").Table;
        _GameAction = _API.Get("GA").Table;
        _GameFunction = _API.Get("GF").Table;
        _Monster = _API.Get("Monster").Table;
        _Card = _API.Get("Card").Table;
        _Func = _API.Get("Func").Table;

        _Monster["AttackByAttributes"] = (Action<int>)API.Monster.AttackByAttributes; 
        _Monster["DefendByAttributes"] = (Action<int>)API.Monster.DefendByAttributes;

        _Card["AttackByAttributes"] = (Action<int, int, CARD_ATTRIBUTE, CARD_ATTRIBUTE, bool>)API.Card.AttackByAttributes;
        _Card["DefendByAttributes"] = (Action<int, int, CARD_ATTRIBUTE>)API.Card.DefendByAttributes;
        _Card["SwapHeroes"] = (Action)API.Card.SwapHeroes;
        _Card["PushMonster"] = (Action<int>)API.Card.PushMonster;
        _Card["PullMonster"] = (Action<int>)API.Card.PullMonster;
        _Card["DrawCardsByCounter"] = (Action<int, CARD_ATTRIBUTE>)API.Card.DrawCardsByCounter;
        _Card["DiscardPlayed"] = (Action<int>)API.Card.DiscardPlayed;
        _Card["AttackExplicit"] = (Action<int, int, int, int, bool>)API.Card.AttackExplicit;
        _Card["SetAttribute"] = (Action<int, CARD_ATTRIBUTE, int>)API.Card.SetAttribute;
        _Card["ActionLink"] = (Action<int, int, CARD_ACTION>)API.Card.ActionLink;
        _Card["DefendExplicit"] = (Action<int, int, int>)API.Card.DefendExplicit;
        _Card["AddStatusByAttributes"] = (Action<int, int, int, CARD_ATTRIBUTE>)API.Card.AddStatusByAttributes;
        _Card["SetMonsterAttribute"] = (Action<int, MONSTER_ATTRIBUTE, int>)API.Card.SetMonsterAttribute;

        _Func["AssertClass"] = (Func<int, bool>)API.Func.AssertClass;
        _Func["GetHeroByCardRange"] = (Func<int, int>)API.Func.GetHeroByCardRange;
        _Func["GetHeroAttribute"] = (Func<int, HERO_ATTRIBUTE, int>)API.Func.GetHeroAttribute;
        _Func["GetCardModelAttribute"] = (Func<int, CARD_ATTRIBUTE, int>)API.Func.GetCardModelAttribute;
        _Func["GetMonsterPositionByIID"] = (Func<int, int>)API.Func.GetMonsterPositionByIID;
        _Func["GetHeroPositionByIID"] = (Func<int, int>)API.Func.GetHeroPositionByIID;
        _Func["GetMonsterIIDByPosition"] = (Func<int, int>)API.Func.GetMonsterIIDByPosition;
        _Func["GetMonsterAttribute"] = (Func<int, MONSTER_ATTRIBUTE, int>)API.Func.GetMonsterAttribute;

        _Func["GetHeroByRange"] = (Func<int, int>)API.Func.GetHeroByRange;
        _Func["GetModelIDByIID"] = (Func<int, int>)API.Func.GetModelIDByIID;

    }

    public static Dictionary<int, CardModel> GetCards()
    {
        Dictionary<int, CardModel> result = new Dictionary<int, CardModel>();

        Table cardsTable = script.Globals.Get("exportCards").Table;
        foreach (DynValue key in cardsTable.Keys)
        {
            Table rawCard = cardsTable.Get(key).Table;
            CardModel cardModel = new CardModel(key, rawCard);
            result.Add(cardModel.id, cardModel);
        }
        return result;
    }

    public static Dictionary<int, StatusModel> GetStatuses()
    {
        Dictionary<int, StatusModel> result = new Dictionary<int, StatusModel>();

        Table statusTable = script.Globals.Get("exportStatuses").Table;
        foreach (DynValue key in statusTable.Keys)
        {
            Table rawStatus = statusTable.Get(key).Table;
            StatusModel statusModel = new StatusModel(key, rawStatus);
            result.Add(statusModel.id, statusModel);
        }
        return result;
    }

    public static Dictionary<int, HeroModel> GetHeroes()
    {
        Dictionary<int, HeroModel> result = new Dictionary<int, HeroModel>();

        Table heroesTable = script.Globals.Get("exportHeroes").Table;
        foreach (DynValue key in heroesTable.Keys)
        {
            Table rawHero= heroesTable.Get(key).Table;
            HeroModel heroModel = new HeroModel(key, rawHero, LuaHelper.GetPlayerDefaults());
            result.Add(heroModel.id, heroModel);
        }
        return result;
    }

    public static PlayerDefaultsModel GetPlayerDefaults()
    {
        Table playerDefaultTable = script.Globals.Get("exportPlayerDefault").Table;
        return new PlayerDefaultsModel(playerDefaultTable);
    }

    public static Dictionary<int, MonsterModel> GetMonsters()
    {
        Dictionary<int, MonsterModel> result = new Dictionary<int, MonsterModel>();

        Table monstersTable = script.Globals.Get("exportMonsters").Table;
        foreach (DynValue key in monstersTable.Keys)
        {
            Table rawMonster = monstersTable.Get(key).Table;
            MonsterModel monsterModel = new MonsterModel(key, rawMonster);
            result.Add(monsterModel.id, monsterModel);
        }
        return result;
    }

    public static Dictionary<MONSTER_INTENTION, string> GetIntentionIcons()
    {
        Dictionary<MONSTER_INTENTION, string> result = new Dictionary<MONSTER_INTENTION, string>();

        Table intentionsTable = script.Globals.Get("exportIntentionsIcons").Table;
        foreach (DynValue key in intentionsTable.Keys)
        {
            result.Add((MONSTER_INTENTION)key.Number, intentionsTable.Get(key).String);
        }
        return result;
    }

    public static Dictionary<int, BattleRoomModel> GetBattleRoomModels()
    {
        Dictionary<int, BattleRoomModel> result = new Dictionary<int, BattleRoomModel>();

        Table battleRoomsTable = script.Globals.Get("exportBattleRooms").Table;
        foreach (DynValue key in battleRoomsTable.Keys)
        {
            Table rawRoom = battleRoomsTable.Get(key).Table;
            BattleRoomModel model = new BattleRoomModel(key, rawRoom);
            result.Add(model.id, model); 
        }
        return result;
    }

    public static Dictionary<int, AdventureModel> GetAdventureModels()
    {
        Dictionary<int, AdventureModel> result = new Dictionary<int, AdventureModel>();

        Table adventuresTable = script.Globals.Get("exportAdventures").Table;
        foreach (DynValue key in adventuresTable.Keys)
        {
            Table rawAdv = adventuresTable.Get(key).Table;
            AdventureModel model = new AdventureModel(key, rawAdv);
            result.Add(model.id, model);
        }
        return result;
    }

    public static GameConstants GetGameConstants()
    {
        return new GameConstants(script.Globals.Get("MAIN_CONSTANTS").Table);
    }
}
