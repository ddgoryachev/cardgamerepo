﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using MoonSharp.Interpreter;


public enum HERO_ATTRIBUTE
{
    UNKNOWN = 0,
    HEALTH = 1,
    MANA = 2,
    HAND = 3,
    DRAW = 4,
    ACTIONS = 5,
    ARMOR = 6,
}

public class HeroInstance: BaseInstance
{
    public readonly int instanceID;
    public readonly HeroModel model;
    public readonly Dictionary<HERO_ATTRIBUTE, int> attributes;
    public List<StatusInstance> statusInstances;
    public GameController gameController;
    internal bool dead;

    public HeroInstance(HeroModel mdl, GameController gC)
    {
        dead = false;
        gameController = gC;

        instanceID = API.InstanceID.Next(this);
        model = mdl;

        attributes = new Dictionary<HERO_ATTRIBUTE, int>();
        foreach (HERO_ATTRIBUTE attr in mdl.attributes.Keys)
        {
            attributes.Add(attr, mdl.attributes[attr]);
        }

        statusInstances = new List<StatusInstance>();
        foreach (int stID in mdl.startingStatuses.Keys)
        {
            statusInstances.Add(gC.GetStatusInstance(stID, mdl.startingStatuses[stID]));
        }
    }

    internal void ReInit()
    {
        SetArmor(model.attributes[HERO_ATTRIBUTE.ARMOR]);
    }

    public List<int> GetHP() {
        return new List<int>() { attributes[HERO_ATTRIBUTE.HEALTH], model.attributes[HERO_ATTRIBUTE.HEALTH]};
    }

    internal void SetHP(int hp)
    {
        attributes[HERO_ATTRIBUTE.HEALTH] = hp;
    }

    internal void AddArmor(int arm, bool fromCard, int range)
    {
        if (!fromCard) attributes[HERO_ATTRIBUTE.ARMOR] += arm;
        else
        {
            attributes[HERO_ATTRIBUTE.ARMOR] += CountArmorFromCard(arm, range);
        }
    }

    public int CountArmorFromCard(int arm, int range)
    {
        int addition = GetStatusAttributesSumm(range == 0 ? STATUS_ATTRIBUTE.M_ARMOR : STATUS_ATTRIBUTE.R_ARMOR);
        float modifier = GetStatusModifiersProduct(range == 0 ? STATUS_MODIFIER.M_ARMOR : STATUS_MODIFIER.R_ARMOR);

        return Mathf.FloorToInt((arm + addition) * modifier);
    }

    public int GetStatusAttributesSumm(STATUS_ATTRIBUTE attr)
    {
        int result = 0;
        statusInstances.ForEach(x => { if (x.attributes.ContainsKey(attr)) result += x.attributes[attr]; });
        return result;
    }

    public float GetStatusModifiersProduct(STATUS_MODIFIER mod)
    {
        float result = 1;
        statusInstances.ForEach(x => { if (x.modifiers.ContainsKey(mod)) result *= x.modifiers[mod]; });
        return Mathf.Max(0, result);
    }

    

    internal int GetArmor()
    {
        return attributes.ContainsKey(HERO_ATTRIBUTE.ARMOR) ? attributes[HERO_ATTRIBUTE.ARMOR] : 0;
    }

    internal void SetArmor(int arm)
    {
        attributes[HERO_ATTRIBUTE.ARMOR] = arm;
    }

    internal int GetAttribute(HERO_ATTRIBUTE attr)
    {
        return attributes[attr];
    }

    internal void AddStatus(int id, int counter)
    {
        StatusInstance st = statusInstances.Find(x => x.model.id == id);
        if (st != null) {
            st.Add(counter); 
        }
        else
        {
            statusInstances.Add(gameController.GetStatusInstance(id, counter));
        }
    }

    internal void StatusCountdown()
    {
        statusInstances.ForEach(x => {
            if (x.HasTag(STATUS_TAG.COUNTDOWN)) x.attributes[STATUS_ATTRIBUTE.COUNTER]--;
        });
        statusInstances = statusInstances.FindAll(x => x.attributes[STATUS_ATTRIBUTE.COUNTER] > 0);
    }

    public float GetFullEvasionFail(int position)
    {
        STATUS_MODIFIER mod = (position > 0) ? STATUS_MODIFIER.R_EVASION : STATUS_MODIFIER.M_EVASION; 
        float result = 1;
        statusInstances.ForEach(x => { if (x.modifiers.ContainsKey(mod)) result *= x.modifiers[mod]; });
        return Mathf.Clamp01(result);
    }

    public override int GetModelID()
    {
        return model.id;
    }
}

public class HeroModel
{
    public readonly int id;
    public readonly string name;

    public readonly string alias;
    public readonly string description;
    public readonly string art;

    public readonly Dictionary<HERO_ATTRIBUTE, int> attributes;
    public readonly List<string> tags;
    public readonly List<int> startingDeck;
    public readonly Dictionary<int, int> startingStatuses;

    public HeroModel(DynValue nm, Table table, PlayerDefaultsModel player)
    {
        name = nm.String;
        id = (int)table.Get("id").Number;
        alias = table.Get("alias").String;
        description = table.Get("description").String;
        art = table.Get("art").String;

        attributes = GetAttributes(table, player);
        tags = GetTags(table);
        startingDeck = GetStartingDeck(table);
        startingStatuses = GetStartingStatuses(table); 
    }

    private Dictionary<int, int> GetStartingStatuses(Table table)
    {
        Dictionary<int, int> result = new Dictionary<int, int>();
        Table rawStatuses = table.Get("startingStatuses").Table;
        foreach (DynValue attribute in rawStatuses.Keys)
        {
            // is it OK?
            result.Add((int)attribute.Number, (int)rawStatuses.Get(attribute).Number);
        }
        return result;
    }

    private List<string> GetTags(Table table)
    {
        List<string> result = new List<string>();
        Table rawTags = table.Get("tags").Table;
        foreach (DynValue tag in rawTags.Values)
        {
            result.Add(tag.String);
        }
        return result;
    }

    private List<int> GetStartingDeck(Table table)
    {
        List<int> result = new List<int>();
        Table rawCardIDs = table.Get("startingDeck").Table;
        foreach (DynValue id in rawCardIDs.Values)
        {
            result.Add((int)id.Number);
        }
        return result;
    }

    private Dictionary<HERO_ATTRIBUTE, int> GetAttributes(Table table, PlayerDefaultsModel player)
    {
        Dictionary<HERO_ATTRIBUTE, int> result = new Dictionary<HERO_ATTRIBUTE, int>();
        Table rawAttributes = table.Get("attributes").Table;
        foreach (DynValue attribute in rawAttributes.Keys)
        {
            HERO_ATTRIBUTE attr = (HERO_ATTRIBUTE)attribute.Number;
            int value = (int)rawAttributes.Get(attribute).Number;
            switch (attr)
            {
                case HERO_ATTRIBUTE.ARMOR: 
                case HERO_ATTRIBUTE.HEALTH: 
                case HERO_ATTRIBUTE.MANA:
                    value = value + player.attributes[attr];
                    break;
            }

            result.Add(attr, value);
        }
        return result;
    }

    
}
