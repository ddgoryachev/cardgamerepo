﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandSettings : MonoBehaviour
{
    public float yPos = -3f;

    public float maxLeftCardPosition = -5f;
    public float maxRightCardPosition = 4f;
    public float maxCardDistance = 1f;
}
