﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckViewSettings : MonoBehaviour
{
    public float startYPos = 0f;
    public float yStep = 4f;

    public float maxLeftCardPosition = -5.5f;
    public float maxRightCardPosition = 5.5f;

    public float scrollSpeed = 1f;
}
