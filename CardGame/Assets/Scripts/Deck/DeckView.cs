﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DeckView : MonoBehaviour
{
    BattleController battleController;

    public DeckViewSettings settings;
    List<CardController> cards = new List<CardController>();

    [SerializeField] Image modal;
    [SerializeField] Image backButton;
    //[SerializeField] Image mask;
    [SerializeField] public Transform holder;
    [SerializeField] GameObject cardViewPrefab;

    private void Start()
    {
        settings = GetComponent<DeckViewSettings>();
        battleController = FindObjectOfType<BattleController>();
    }

    public void Show(List<CardInstance> cardInstances, bool sortByDeck)
    {
        modal.gameObject.SetActive(true);
        //mask.gameObject.SetActive(true);

        List<CardModel> cardsToShow = GetCardsOrder(cardInstances, sortByDeck);

        ShowCards(cardsToShow);
        DistributeCards();

        holder.gameObject.SetActive(true);
        backButton.gameObject.SetActive(true);
    }

    public void Close()
    {
        holder.gameObject.SetActive(false);

        cards.ForEach(card => Destroy(card.gameObject));
        cards = new List<CardController>();

        //mask.gameObject.SetActive(false);
        modal.gameObject.SetActive(false);
        backButton.gameObject.SetActive(false);
    }

    private List<CardModel> GetCardsOrder(List<CardInstance> cardInstances, bool sortByDeck)
    {
        if (!battleController) battleController = FindObjectOfType<BattleController>();

        List<CardModel> cardsToShow = new List<CardModel>();
        if (sortByDeck)
        {
            foreach (CardInstance card in battleController.deck)
            {
                if (cardInstances.Contains(card)) cardsToShow.Add(card.model);
            }
            
        }
        else cardInstances.ForEach(card => cardsToShow.Add(card.model));
        return cardsToShow;
    }

    private void ShowCards(List<CardModel> cardsToShow)
    {
        foreach (CardModel model in cardsToShow)
        {
            CardInstance card = new CardInstance(model);
            GameObject newCard = Instantiate(cardViewPrefab, holder.transform);

            CardController cardController = newCard.GetComponent<CardController>();
            cardController.Init(card, CARD_BEHAVIOR.DECK);
            cards.Add(cardController);
        }
    }

    public void DistributeCards()
    {
        float width = settings.maxRightCardPosition - settings.maxLeftCardPosition;
        float step = width / 4;

        for (int i = 0; i<cards.Count; i++)
        {
            float x = settings.maxLeftCardPosition + step * (i % 5);
            float y = settings.startYPos - settings.yStep * (i / 5);
            cards[i].transform.position = new Vector2(x,y);
            cards[i].transform.SetParent(holder);
        }
    }
}
