﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MoonSharp.Interpreter;

public class BattleRoomInstance
{
    public BattleRoomModel model;

    public BattleRoomInstance(BattleRoomModel mdl)
    {
        model = mdl;
    }
}

public class BattleRoomModel
{
    public readonly int id;
    public readonly string name;

    public readonly string backArt;
    public readonly List<int> monsters = new List<int>();


    public BattleRoomModel(DynValue nm, Table table)
    {
        name = nm.String;
        id = (int)table.Get("id").Number;

        backArt = table.Get("backArt").String;
        monsters = GetMonsters(table);
    }

    List<int> GetMonsters(Table table)
    {
        List<int> result = new List<int>();
        Table rawIDs = table.Get("monsters").Table;
        foreach (DynValue value in rawIDs.Values)
        {
            result.Add((int)value.Number);
        }
        return result;
    }
}
