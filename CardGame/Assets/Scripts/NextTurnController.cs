﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.EventSystems;

public class NextTurnController : MonoBehaviour
{
    public BattleController gameController;
    [SerializeField] Button button;

    private void Start()
    {
        gameController = FindObjectOfType<BattleController>();
        button = GetComponent<Button>();
    }

    public void SetOn(bool v)
    {
        gameObject.SetActive(v);
        
    }

    public void EndTurn()
    {
        gameController.EndTurn();
    }
}
