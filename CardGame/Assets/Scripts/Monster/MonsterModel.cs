﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using MoonSharp.Interpreter;


public enum MONSTER_ATTRIBUTE
{
    UNKNOWN = 0,
    HEALTH = 1,
    ARMOR = 2,
    DAMAGE = 3,
    DEFENCE = 4,
    INCOMING_DAMAGE = 5,
}

public enum MODIFIER
{
    DAMAGE = 1,
    INCOMING_DAMAGE = 2,
}

public enum MONSTER_INTENTION
{
    UNKNOWN = 0,
    MELEE = 1,
    RANGED = 2,
    DEFEND_SELF = 3,
    WAIT = 4
}

public enum ACTION_ATTRIBUTE
{
    UNKNOWN = 0,
    DAMAGE = 1,
    MULTIPLIER = 2,
    DEFENCE = 3
}

public class MonsterAction
{
    public string name = "";
    public MONSTER_INTENTION intention;
    public Action<int> action;
    public Dictionary<ACTION_ATTRIBUTE, int> attributes;

    public MonsterAction(string name, MONSTER_INTENTION intention, Action<int> action, Dictionary<ACTION_ATTRIBUTE, int> attributes)
    {
        this.intention = intention;
        this.action = action;
        this.attributes = attributes;
    }
}

public class MonsterInstance: BaseInstance
{
    public readonly int instanceID;
    public readonly MonsterModel model;
    public readonly Dictionary<MONSTER_ATTRIBUTE, int> attributes;
    public string preparedAction;
    public HashSet<string> markers = new HashSet<string>();
    public List<StatusInstance> statusInstances;
    public GameController gameController;
    internal bool dead;

    public MonsterInstance(MonsterModel mdl, GameController gC)
    {
        dead = false;
        gameController = gC;

        instanceID = API.InstanceID.Next(this);
        model = mdl;
        attributes = new Dictionary<MONSTER_ATTRIBUTE, int>();
        foreach (MONSTER_ATTRIBUTE attr in mdl.attributes.Keys)
        {
            attributes.Add(attr, mdl.attributes[attr]);
        }
        preparedAction = model.tactics(0, "");

        statusInstances = new List<StatusInstance>();
        foreach (int stID in mdl.startingStatuses.Keys)
        {
            statusInstances.Add(gC.GetStatusInstance(stID, mdl.startingStatuses[stID]));
        }
    }

    public int GetStatusAttributesSumm(STATUS_ATTRIBUTE attr)
    {
        int result = 0;
        statusInstances.ForEach(x => { if (x.attributes.ContainsKey(attr)) result += x.attributes[attr]; });
        return result;
    }

    public float GetStatusModifiersProduct(STATUS_MODIFIER mod)
    {
        float result = 1;
        statusInstances.ForEach(x => {
            if (x.modifiers.ContainsKey(mod)) {
                result *= x.modifiers[mod];
            }
        });
        return Mathf.Max(result, 0);
    }

    public float GetFullEvasionFail(int position)
    {
        STATUS_MODIFIER mod = (position > 0) ? STATUS_MODIFIER.R_EVASION : STATUS_MODIFIER.M_EVASION; 
        float result = 1;
        statusInstances.ForEach(x => { if (x.modifiers.ContainsKey(mod)) result *= x.modifiers[mod]; });
        return Mathf.Clamp01(result);
    }

    public void DoStatusActions(STATUS_ACTION act)
    {
        statusInstances.ForEach(x => { x.DoAction(act); });
    }

    public MonsterAction GetPreparedAction()
    {
        return model.actions[preparedAction];
    }

    public int GetPreparedActionDamage(CharController target)
    {
        int actionDMG = GetPreparedAction().attributes[ACTION_ATTRIBUTE.DAMAGE];

        int pos = API.Func.GetMonsterPositionByIID(this.instanceID);

        actionDMG = API.NoAPI.CalcMonsterDamage(actionDMG, target.GetPos(), pos, target.hero, this);

        return actionDMG;
    }

    public void ChooseNextAction(int turn)
    {
        preparedAction = model.tactics(turn+1, preparedAction);
    }

    public List<int> GetHP()
    {
        return new List<int>() { attributes[MONSTER_ATTRIBUTE.HEALTH], model.attributes[MONSTER_ATTRIBUTE.HEALTH] };
    }

    internal void SetHP(int hp)
    {
        attributes[MONSTER_ATTRIBUTE.HEALTH] = hp;
    }

    internal int GetArmor()
    {
        return attributes.ContainsKey(MONSTER_ATTRIBUTE.ARMOR) ? attributes[MONSTER_ATTRIBUTE.ARMOR] : 0;
    }

    internal void SetArmor(int arm)
    {
        attributes[MONSTER_ATTRIBUTE.ARMOR] = arm;
    }

    internal void AddStatus(int id, int counter)
    {
        StatusInstance st = statusInstances.Find(x => x.model.id == id);
        if (st != null)
        {
            st.Add(counter);
        }
        else
        {
            statusInstances.Add(gameController.GetStatusInstance(id, counter));
        }
    }

    internal void AddArmor(int arm)
    {
        attributes[MONSTER_ATTRIBUTE.ARMOR] += arm;
    }

    internal void StatusCountdown()
    {
        statusInstances.ForEach(x => {
            if (x.HasTag(STATUS_TAG.COUNTDOWN)) x.attributes[STATUS_ATTRIBUTE.COUNTER]--;
        });
        statusInstances = statusInstances.FindAll(x => x.attributes[STATUS_ATTRIBUTE.COUNTER] > 0);
    }

    internal int GetAttribute(MONSTER_ATTRIBUTE attr)
    {
        return attributes[attr];
    }

    internal void SetAttribute(MONSTER_ATTRIBUTE attr, int value)
    {
        attributes[attr] = value;
    }

    public override int GetModelID()
    {
        return model.id;
    }
}

public class MonsterModel
{
    public readonly int id;
    public readonly string name;

    public readonly string alias;
    public readonly string description;
    public readonly string art;
    public readonly int intentionHeight;

    public readonly Dictionary<MONSTER_ATTRIBUTE, int> attributes;
    public readonly List<string> tags;
    public readonly Dictionary<string, MonsterAction> actions;
    public readonly Func<int, string, string> tactics;
    public readonly Dictionary<int, int> startingStatuses;

    public MonsterModel(DynValue nm, Table table)
    {
        name = nm.String;
        id = (int)table.Get("id").Number;
        alias = table.Get("alias").String;
        description = table.Get("description").String;
        art = table.Get("art").String;
        intentionHeight = (int)table.Get("intentionHeight").Number;

        attributes = GetAttributes(table);
        tags = GetTags(table);
        actions = GetActions(table);
        tactics = GetTactics(table);
        startingStatuses = GetStartingStatuses(table);
    }

    private Dictionary<int, int> GetStartingStatuses(Table table)
    {
        Dictionary<int, int> result = new Dictionary<int, int>();
        Table rawStatuses = table.Get("startingStatuses").Table;
        foreach (DynValue attribute in rawStatuses.Keys)
        {
            // is it OK?
            result.Add((int)attribute.Number, (int)rawStatuses.Get(attribute).Number);
        }
        return result;
    }

    private Func<int, string, string> GetTactics(Table table)
    {
        return (int turn, string lastAttack) => LuaHelper.script.Call(table.Get("tactics"), turn, lastAttack).String;
    }

    private List<string> GetTags(Table table)
    {
        List<string> result = new List<string>();
        Table rawTags = table.Get("tags").Table;
        foreach (DynValue tag in rawTags.Values)
        {
            result.Add(tag.String);
        }
        return result;
    }

    private Dictionary<string, MonsterAction> GetActions(Table table)
    {
        Dictionary<string, MonsterAction> result = new Dictionary<string, MonsterAction>();
        Table rawActions = table.Get("actions").Table;
        foreach (DynValue action in rawActions.Keys)
        {
            Action<int> newAction = (int iID) => { LuaHelper.script.Call(rawActions.Get(action).Table.Get("action"), iID); };
            MONSTER_INTENTION intention = (MONSTER_INTENTION)(int)rawActions.Get(action).Table.Get("intention").Number;
            Dictionary<ACTION_ATTRIBUTE, int> actionAttributes = GetActionAttributes(rawActions.Get(action).Table.Get("attributes").Table);
            MonsterAction monsterAction = new MonsterAction(action.String, intention, newAction, actionAttributes);
            result.Add(action.String, monsterAction);
        }
        return result;
    }

    private Dictionary<MONSTER_ATTRIBUTE, int> GetAttributes(Table table)
    {
        Dictionary<MONSTER_ATTRIBUTE, int> result = new Dictionary<MONSTER_ATTRIBUTE, int>();
        Table rawAttributes = table.Get("attributes").Table;
        foreach (DynValue attribute in rawAttributes.Keys)
        {
            // is it OK?
            result.Add((MONSTER_ATTRIBUTE)attribute.Number, (int)rawAttributes.Get(attribute).Number);
        }
        return result;
    }

    private Dictionary<ACTION_ATTRIBUTE, int> GetActionAttributes(Table table)
    {
        Dictionary<ACTION_ATTRIBUTE, int> result = new Dictionary<ACTION_ATTRIBUTE, int>();
        foreach (DynValue attribute in table.Keys)
        {
            // is it OK?
            result.Add((ACTION_ATTRIBUTE)attribute.Number, (int)table.Get(attribute).Number);
        }
        return result;
    }
}
