﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatusView : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] TextMeshProUGUI text;

    public void UpdateView(StatusInstance statusInstance)
    {

        image.sprite = Resources.Load<Sprite>(statusInstance.model.art);

        var attr = statusInstance.attributes;
        if (statusInstance.model.tags.Contains(STATUS_TAG.COUNTDOWN))
        {
            text.enabled = true;
            text.text = attr[STATUS_ATTRIBUTE.COUNTER].ToString();
        }
        else
        {
            text.enabled = false;
        }
    }

}


