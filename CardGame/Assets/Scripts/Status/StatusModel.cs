﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using MoonSharp.Interpreter;

public enum STATUS_ATTRIBUTE
{
    UNKNOWN = 0,
    COUNTER = 1, // как долго действует статус

    M_DAMAGE = 2, // дополнительный наносимый урон на первой позиции
    M_ARMOR = 3, // дополнительная получаемая броня на первой позиции
    M_INCOMING = 4, // дополнительный входящий урон на первой позиции

    R_DAMAGE = 5, // дополнительный наносимый урон на второй позиции (для монстров - на 2-3 позиции)
    R_ARMOR = 6, // дополнительная получаемая броня на второй позиции (для монстров - на 2-3 позиции)
    R_INCOMING = 7, // дополнительный входящий урон на второй позиции (для монстров - на 2-3 позиции)
}

public enum STATUS_MODIFIER
{
    UNKNOWN = 0,

    M_DAMAGE = 1, // множитель урона на первой позиции
    M_ARMOR = 2, // множитель брони на первой позиции
    M_ACCURACY = 3, // шанс попадания на первой позиции
    M_EVASION = 4, // шанс увернуться на первой позиции
    M_INCOMING = 5, // множитель входящего урона на первой позиции

    R_DAMAGE = 6, // множитель урона на второй позиции (для монстров - на 2-3 позиции)
    R_ARMOR = 7, // множитель брони на второй позиции (для монстров - на 2-3 позиции)
    R_ACCURACY = 8, // шанс попадания на второй позиции (для монстров - на 2-3 позиции)
    R_EVASION = 9, // шанс увернуться на второй позиции (для монстров - на 2-3 позиции)
    R_INCOMING = 10, // множитель входящего урона на второй позиции (для монстров - на 2-3 позиции)
}

public enum STATUS_TAG
{
    STACKABLE = 1, // при повторном добавлении статуса каунтеры складываются
    COUNTDOWN = 2, // каунтер статуса уменьшается каждый ход
    PRECISION = 3, // носитель всегда попадает 
}

public enum STATUS_ACTION
{
    UNKNOWN = 0,

    ON_ADD = 1,
    ON_REMOVE = 2,

    ON_TURN_START = 3,
    ON_TURN_END = 4,

    ON_CARD_DRAW = 5,
    ON_CARD_PLAY = 6,
    ON_CARD_DISCARD = 7,
    ON_CARD_EXILE = 8,

    ON_DAMAGE_RECIEVED = 9,
    ON_ARMOR_ADD = 10,
    ON_HEALTH_DAMAGE_RECEIVED = 11,

    ON_CARD_CREATE = 12,

}



public class StatusInstance : BaseInstance
{
    public readonly int instanceID;
    public readonly StatusModel model;
    public readonly Dictionary<STATUS_ATTRIBUTE, int> attributes;
    public readonly Dictionary<STATUS_MODIFIER, float> modifiers;
    public readonly CharController owner;

    public StatusInstance(StatusModel mdl, int counter)
    {
        instanceID = API.InstanceID.Next(this);
        model = mdl;
        attributes = new Dictionary<STATUS_ATTRIBUTE, int>();
        foreach (STATUS_ATTRIBUTE attr in mdl.attributes.Keys)
        {
            attributes.Add(attr, mdl.attributes[attr]);
        }

        if (attributes.ContainsKey(STATUS_ATTRIBUTE.COUNTER)) attributes[STATUS_ATTRIBUTE.COUNTER] = counter;

        modifiers = new Dictionary<STATUS_MODIFIER, float>();
        foreach (STATUS_MODIFIER mod in mdl.modifiers.Keys)
        {
            modifiers.Add(mod, mdl.modifiers[mod]);
        }
    }

    public void DoAction(STATUS_ACTION action)
    {
        if (model.actions.ContainsKey(action))
        {
            model.actions[action](instanceID);
        }
    }

    public bool HasTag(STATUS_TAG tag)
    {
        return  model.tags.FindIndex(x => x == tag) != -1;
    }

    public void Add(int counter)
    {
        if (HasTag(STATUS_TAG.STACKABLE))
            attributes[STATUS_ATTRIBUTE.COUNTER] = attributes[STATUS_ATTRIBUTE.COUNTER] + counter;
    }

    public string GetDescription()
    {
        string result = "<color=#888888>IID: " + instanceID + "</color> " + "<b>" + model.alias + "</b>";

        string desc = model.description;

        foreach (STATUS_MODIFIER key in modifiers.Keys)
        {
            int k = (int)key;
            desc = desc.Replace("M{" + k + "}", (Mathf.RoundToInt(modifiers[key] * 100)).ToString());
        }

        foreach (STATUS_ATTRIBUTE key in attributes.Keys)
        {
            desc = desc.Replace("{" + key + "}", attributes[key].ToString());
        }

        return result + " " + desc; 
    }

    public override int GetModelID()
    {
        return model.id;
    }
}

public class StatusModel
{
    public readonly int id;
    public readonly string name;

    public readonly string alias;
    public readonly string description;

    public readonly Dictionary<STATUS_ATTRIBUTE, int> attributes;
    public readonly Dictionary<STATUS_MODIFIER, float> modifiers;
    public readonly List<STATUS_TAG> tags;

    public readonly Dictionary<STATUS_ACTION, Action<int>> actions;

    public readonly string art;
    public readonly Color color;

    public StatusModel(DynValue nm, Table table)
    {
        name = nm.String;
        id = (int)table.Get("id").Number;
        alias = table.Get("alias").String;
        description = table.Get("description").String;

        attributes = GetAttributes(table);
        modifiers = GetModifiers(table);
        tags = GetTags(table);
        art = table.Get("art").String;
        ColorUtility.TryParseHtmlString(table.Get("color").String, out color);

        actions = GetActions(table);
    }

    private Dictionary<STATUS_ATTRIBUTE, int> GetAttributes(Table table)
    {
        Dictionary<STATUS_ATTRIBUTE, int> result = new Dictionary<STATUS_ATTRIBUTE, int>();
        Table rawAttributes = table.Get("attributes").Table;
        foreach (DynValue attribute in rawAttributes.Keys)
        {
            // is it OK?
            result.Add((STATUS_ATTRIBUTE)(int)attribute.Number, (int)rawAttributes.Get(attribute).Number);
        }
        return result;
    }

    private Dictionary<STATUS_MODIFIER, float> GetModifiers(Table table)
    {
        Dictionary<STATUS_MODIFIER, float> result = new Dictionary<STATUS_MODIFIER, float>();
        Table rawMods = table.Get("modifiers").Table;
        foreach (DynValue attribute in rawMods.Keys)
        {
            // is it OK?
            result.Add((STATUS_MODIFIER)(int)attribute.Number, (float)rawMods.Get(attribute).Number);
        }
        return result;
    }

    private List<STATUS_TAG> GetTags(Table table)
    {
        List<STATUS_TAG> result = new List<STATUS_TAG>();
        Table rawTags = table.Get("tags").Table;
        foreach (DynValue tag in rawTags.Values)
        {
            result.Add((STATUS_TAG)(int)tag.Number);
        }
        return result;
    }

    private Dictionary<STATUS_ACTION, Action<int>> GetActions(Table table)
    {
        Dictionary<STATUS_ACTION, Action<int>> result = new Dictionary<STATUS_ACTION, Action<int>>();
        Table rawActions = table.Get("actions").Table;
        foreach (DynValue action in rawActions.Keys)
        {
            Action<int> newAction = (iID) => { LuaHelper.script.Call(rawActions.Get(action), iID);};
            result.Add((STATUS_ACTION)(int)action.Number, newAction);
        }
        return result;
    }
}
