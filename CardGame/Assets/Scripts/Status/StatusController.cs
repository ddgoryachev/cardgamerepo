﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StatusController : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    public StatusInstance statusInstance;
    [SerializeField] StatusView statusView;
    [SerializeField] StatusHint hint;

    bool popupTimeout = false;

    private void Start()
    {
        hint = FindObjectOfType<StatusHint>();
    }

    public void UpdateView(StatusInstance status)
    {
        statusInstance = status;
        gameObject.SetActive(true);
        statusView.UpdateView(status);
    }

    public void Clear()
    {
        gameObject.SetActive(false);
        statusInstance = null;
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        hint.Hide();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        hint.Show(statusInstance.GetDescription());
    }


}
