﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MoonSharp.Interpreter;

public class AdventureInstance
{
    public AdventureModel model;
    public int currentRoomIndex;

    public AdventureInstance(AdventureModel mdl)
    {
        model = mdl;
        currentRoomIndex = -1;
    }

    public int Current()
    {
        return model.rooms[currentRoomIndex % model.rooms.Count];
    }

    public int Next()
    {
        currentRoomIndex++;
        return Current();
    }

    public string GetInfoString()
    {
        return "Room ID: " + Current() + " RoomIndex: " + currentRoomIndex + " Adv ID: " + model.id;
    }
}

public class AdventureModel
{
    public readonly int id;
    public readonly string name;

    public readonly List<int> rooms = new List<int>();

    public AdventureModel(DynValue nm, Table table)
    {
        name = nm.String;
        id = (int)table.Get("id").Number;

        rooms = GetRooms(table);
    }

    List<int> GetRooms(Table table)
    {
        List<int> result = new List<int>();
        Table rawIDs = table.Get("rooms").Table;
        foreach (DynValue value in rawIDs.Values)
        {
            result.Add((int)value.Number);
        }
        return result;
    }
}
