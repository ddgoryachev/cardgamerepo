﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum PILE_TYPE
{
    UNKNOWN,
    DRAW,
    DISCARD,
    EXILE,
    DECK
}

public class PileController : MonoBehaviour, IPointerDownHandler
{
    public BattleController battleController;
    public BattleDeckController battleDeckController;
    public DeckView deckView;
    [SerializeField] public PILE_TYPE pileType;
    [SerializeField] PileView pileView;

    private void Start()
    {
        battleController = FindObjectOfType<BattleController>();
        battleDeckController = FindObjectOfType<BattleDeckController>();
        deckView = battleController.deckContentsController.deckView;
        
        pileView = GetComponent<PileView>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {

        switch (pileType)
        {
            case PILE_TYPE.DRAW:
                deckView.Show(battleDeckController.drawPile, true);
                break;
            case PILE_TYPE.DISCARD:
                deckView.Show(battleDeckController.discardPile, false);
                break;
            case PILE_TYPE.EXILE:
                deckView.Show(battleDeckController.exilePile, false);
                break;
            case PILE_TYPE.DECK:
                deckView.Show(battleController.deck, false);
                break;
        }
    }

    public void ViewUpdate()
    {
        pileView.ViewUpdate();
    } 
}
