﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class PileView : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    PileController pileController;
    PILE_TYPE pileType;

    void Start()
    {
        pileController = GetComponent<PileController>();
        pileType = pileController.pileType;
    }

    internal void ViewUpdate()
    {
        switch (pileType)
        {
            case PILE_TYPE.DRAW:
                text.text = pileController.battleDeckController.drawPile.Count.ToString();
                break;
            case PILE_TYPE.DISCARD:
                text.text = pileController.battleDeckController.discardPile.Count.ToString();
                break;
            case PILE_TYPE.EXILE:
                text.text = pileController.battleDeckController.exilePile.Count.ToString();
                break;
            case PILE_TYPE.DECK:
                text.text = pileController.battleController.deck.Count.ToString();
                break;
            default:
                Debug.Log("pileType ERROR");
                break;
        }
        
    }
}
