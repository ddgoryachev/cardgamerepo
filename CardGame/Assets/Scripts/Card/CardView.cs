﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using System;

public class CardView : MonoBehaviour
{
    public CardInstance card;
    CardController cardController;
    GameController gameController;

    [SerializeField] Image artBack;
    [SerializeField] Image art;
    [SerializeField] Image plate;
    [SerializeField] Image frame;
    [SerializeField] Image plaque;
    [SerializeField] Image mana;
    [SerializeField] Image action;
    [SerializeField] TextMeshProUGUI manaText;
    [SerializeField] TextMeshProUGUI actionText;
    [SerializeField] TextMeshProUGUI nameText;
    [SerializeField] TextMeshProUGUI descText;

    [SerializeField] Outline outline;   
    [SerializeField] Outline manaOutline;   
    [SerializeField] Outline actionOutline;

    [SerializeField] TextMeshProUGUI iidText;
    private int order;

    public void InitView()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        cardController = GetComponent<CardController>();
        gameController = FindObjectOfType<GameController>();

        card = cardController.cardInstance;

        artBack.sprite = GetPartImage(CARD_PART.ART_BACK, "Card/ArtBack");
        art.sprite = GetPartImage(CARD_PART.ART, "Card/Art");
        plate.sprite = GetPartImage(CARD_PART.PLATE, "Card/Plate");
        frame.sprite = GetPartImage(CARD_PART.FRAME, "Card/Frame");
        plaque.sprite = GetPartImage(CARD_PART.PLAQUE, "Card/Plaque");
        mana.sprite = GetPartImage(CARD_PART.MANA_ART, "Card/Circle");
        action.sprite = GetPartImage(CARD_PART.ACTION_ART, "Card/Circle");

        artBack.color = GetColor(CARD_COLOR.ART_BACK, "#FFFFFF");
        art.color = GetColor(CARD_COLOR.ART, "#FFFFFF");
        plate.color = GetColor(CARD_COLOR.PLATE, "#FFFFFF");
        frame.color = GetColor(CARD_COLOR.FRAME, "#FFFFFF");
        plaque.color = GetColor(CARD_COLOR.PLAQUE, "#FFFFFF");
        mana.color = GetColor(CARD_COLOR.MANA_ART, "#FFFFFF");
        action.color = GetColor(CARD_COLOR.ACTION_ART, "#FFFFFF");
        manaText.color = GetColor(CARD_COLOR.MANA_TEXT, "#FFFFFF");
        actionText.color = GetColor(CARD_COLOR.ACTION_TEXT, "#FFFFFF");
        nameText.color = GetColor(CARD_COLOR.NAME_TEXT, "#FFFFFF");
        descText.color = GetColor(CARD_COLOR.DESC_TEXT, "#FFFFFF");

        if (!card.attributes.ContainsKey(CARD_ATTRIBUTE.MANA_COST))
        {
            manaText.enabled = false;
            mana.enabled = false;
        }

        if (!card.attributes.ContainsKey(CARD_ATTRIBUTE.ACTION_COST))
        {
            actionText.enabled = false;
            action.enabled = false;
        }

        if (cardController.behavior == CARD_BEHAVIOR.DECK) GetComponent<Canvas>().sortingOrder = 200;

        UpdateView();

    }
    
    public void UpdateView()
    {
        manaText.text = GetAttribute(CARD_ATTRIBUTE.MANA_COST).ToString();
        actionText.text = GetAttribute(CARD_ATTRIBUTE.ACTION_COST).ToString();
        nameText.text = MakeString(card.model.alias);
        descText.text = MakeDescString();
        iidText.text = "IID: " + card.instanceID.ToString();
    }

    private void Update()
    {
        //UpdateView();
    }

    private Sprite GetPartImage(CARD_PART part, string def)
    {
        string path = card.model.parts.ContainsKey(part)
            ? card.model.parts[part]
            : def;

        return Resources.Load<Sprite>(path);
    }

    internal void UpdateOrder()
    {
        
    }

    

    private Color GetColor(CARD_COLOR color, string def)
    {
        return card.model.colors.ContainsKey(color)
            ? card.model.colors[color]
            : ParseColor(def);
    }

    private Color ParseColor(string str)
    {
        Color color = Color.white;
        ColorUtility.TryParseHtmlString(str, out color);
        return color;
    }

    private string MakeString(string str)
    {
        foreach (CARD_ATTRIBUTE attr in card.attributes.Keys)
        {
            int value = GetAttribute(attr);
            switch (attr) {
                case CARD_ATTRIBUTE.ARMOR:
                    value = card.markers.Contains("NO_ARMOR") ? 0 : value;
                    break;
            }
            int diff = value - GetModelAttribute(attr);
            string tag = "<color=#222222>";
            if (diff > 0) tag = "<color=#448844>";
            else if (diff < 0) tag = "<color=#883333>";

            str = str.Replace("{" + (int)attr + "}", "<b>" + tag + value.ToString() + "</color></b>");
        }
        return str;
    }

    private string MakeDescString()
    {
        string result = (card.model.range == 0) ? "<b>MELEE</b>" : "<b>RANGED</b>";
        result = result + " <b>" + GetTargetAlias(card.model.cardTarget) + "</b>";
        result = result + Environment.NewLine;

        string str = card.model.description;

        str = str.Replace("{HERO}", Environment.NewLine + "<b>" + gameController.GetHeroNameById(card.model.heroID).ToUpper() + "</color></b>");

        foreach (CARD_ATTRIBUTE attr in card.attributes.Keys)
        {
            int value = GetAttribute(attr);
            int diff = value - GetModelAttribute(attr);
            string tag = "<color=#222222>";
            if (diff > 0) tag = "<color=#448844>";
            else if (diff < 0) tag = "<color=#883333>";

            str = str.Replace("{" + (int)attr + "}", "<b>" + tag + value.ToString() + "</color></b>");
        }

        result = result + str;
        return result;
    }

    private string GetTargetAlias(CARD_TARGET cardTarget)
    {
        switch (cardTarget)
        {
            case CARD_TARGET.ALL:
                return "ALL";
            case CARD_TARGET.ANY:
                return "ANY";
            case CARD_TARGET.CLOSEST:
                return "CLOSEST";
            case CARD_TARGET.SELF:
                return "SELF";
            case CARD_TARGET.RANDOM:
                return "RANDOM";
        }
        return "";
    }

    private int GetAttribute(CARD_ATTRIBUTE attr)
    {
        return card.attributes.ContainsKey(attr) ?
            card.attributes[attr] : 0;
    }

    private int GetModelAttribute(CARD_ATTRIBUTE attr)
    {
        return card.model.attributes.ContainsKey(attr) ?
            card.model.attributes[attr] : 0;
    }

    public void ReadyToPlay(bool ready)
    {
        outline.enabled = ready;
    }

    public void PointsHightlight(bool enoughPoints)
    {
        actionOutline.enabled = !enoughPoints;
    }
}
