﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using MoonSharp.Interpreter;

public enum CARD_TARGET
{
    NO_ONE = 0,
    ALL = 1,
    CLOSEST = 2,
    ANY = 3,
    SELF = 4,
    RANDOM = 5
}

public enum CARD_ATTRIBUTE
{
    UNKNOWN = 0,
    MANA_COST = 1, // отображается на значке маны
    ACTION_COST = 2, // отображается на значке экшена
    DAMAGE = 3, // базовый урон карты
    ARMOR = 4, // базовый армор карты
    MULTIPLIER = 5, // в общем случае - сколько раз будет нанесён урон
    PENALTY = 6, // ????
    COUNTER = 7, // каунтер накладываемого статуса
    B_DAMAGE = 8, // бонусный урон
    B_MULTIPLIER = 9, // бонусный мультипликатор
    B_ARMOR = 10, // бонусный армор
    BONUS = 11, // всё, что можно добавить
}

public enum CARD_ACTION
{
    UNKNOWN = 0,
    ON_DRAW = 1, // эффект карты, срабатывающий, когда она берётся из колоды
    ON_PLAY = 2, // основной эффект, при разыгрывании
    ON_DISCARD = 3, // эффект при сбросе карты
    ON_EXILE = 4, // эффект при изгнании карты
    ON_HEROES_SWAP = 5, // эффект при перемещении героев
    ON_CHAR_DEATH = 6, // эффект при изгнании карты
    AFTER_PLAY = 7
}

public enum CARD_PART
{
    UNKNOWN = 0,
    ART = 1,
    ART_BACK = 2,
    PLATE = 3,
    PLAQUE = 4,
    FRAME = 5,
    ACTION_ART = 6,
    MANA_ART = 7,
}

public enum CARD_COLOR
{
    UNKNOWN = 0,
    ART = 1,
    ART_BACK = 2,
    PLATE = 3,
    PLAQUE = 4,
    FRAME = 5,
    ACTION_ART = 6,
    MANA_ART = 7,
    ACTION_TEXT = 8,
    MANA_TEXT = 9,
    NAME_TEXT = 10,
    DESC_TEXT = 11,
}

public enum RANGE
{
    RANGED = 1,
    MELEE = 2,
}

[System.Serializable]
public class CardInstance: BaseInstance
{
    public readonly int instanceID;
    public readonly CardModel model;
    public readonly Dictionary<CARD_ATTRIBUTE, int> attributes;
    public readonly HashSet<string> markers = new HashSet<string>();

    public CardInstance(CardModel mdl)
    {
        instanceID = API.InstanceID.Next(this);
        model = mdl;
        attributes = new Dictionary<CARD_ATTRIBUTE, int>();
        foreach (CARD_ATTRIBUTE attr in mdl.attributes.Keys)
        {
            attributes.Add(attr, mdl.attributes[attr]);
        }
    }

    public void DoAction(CARD_ACTION act, int targetIID)
    {
        Debug.Log($"{act} {targetIID}");
        if (model.actions.ContainsKey(act)) model.actions[act](instanceID, targetIID); 
    }

    public int GetAttribute(CARD_ATTRIBUTE attr)
    {
        if (attributes.ContainsKey(attr)) return attributes[attr];
        else
        {
            Dictionary<CARD_ATTRIBUTE, int> defaults = new Dictionary<CARD_ATTRIBUTE, int>()
            {
                {CARD_ATTRIBUTE.MANA_COST, 0},
                {CARD_ATTRIBUTE.ACTION_COST, 0},
                {CARD_ATTRIBUTE.DAMAGE, 0},
                {CARD_ATTRIBUTE.ARMOR, 0},
                {CARD_ATTRIBUTE.MULTIPLIER, 1},
                {CARD_ATTRIBUTE.PENALTY, 0},
                {CARD_ATTRIBUTE.COUNTER, 1},
                {CARD_ATTRIBUTE.B_DAMAGE, 0},
                {CARD_ATTRIBUTE.B_MULTIPLIER, 1},
                {CARD_ATTRIBUTE.B_ARMOR, 0},
                {CARD_ATTRIBUTE.BONUS, 0},
            };
            return defaults[attr];
        }
    }

    public int GetModelAttribute(CARD_ATTRIBUTE attr)
    {
        if (model.attributes.ContainsKey(attr)) return model.attributes[attr];
        else
        {
            Dictionary<CARD_ATTRIBUTE, int> defaults = new Dictionary<CARD_ATTRIBUTE, int>()
            {
                {CARD_ATTRIBUTE.MANA_COST, 0},
                {CARD_ATTRIBUTE.ACTION_COST, 0},
                {CARD_ATTRIBUTE.DAMAGE, 0},
                {CARD_ATTRIBUTE.ARMOR, 0},
                {CARD_ATTRIBUTE.MULTIPLIER, 1},
                {CARD_ATTRIBUTE.PENALTY, 0},
                {CARD_ATTRIBUTE.COUNTER, 1},
                {CARD_ATTRIBUTE.B_DAMAGE, 0},
                {CARD_ATTRIBUTE.B_MULTIPLIER, 1},
                {CARD_ATTRIBUTE.B_ARMOR, 0},
                {CARD_ATTRIBUTE.BONUS, 0},
            };
            return defaults[attr];
        }
    }

    public override int GetModelID()
    {
        return model.id;
    }
}

public class CardModel
{
    public readonly int id;
    public readonly string name;

    public readonly string alias;
    public readonly string description;

    public readonly Dictionary<CARD_ATTRIBUTE, int> attributes;
    public readonly List<string> tags;
    public readonly CARD_TARGET cardTarget;
    public readonly int range;
    public readonly int heroID;

    public readonly Dictionary<CARD_ACTION, Action<int, int>> actions;

    public readonly Dictionary<CARD_PART, string> parts;
    public readonly Dictionary<CARD_COLOR, Color> colors;

    public CardModel(DynValue nm, Table table)
    {
        name = nm.String;
        id = (int)table.Get("id").Number;
        alias = table.Get("alias").String;
        description = table.Get("description").String;

        attributes = GetAttributes(table);
        tags = GetTags(table);
        cardTarget = (CARD_TARGET)(int)table.Get("cardTarget").Number;
        range = (int)table.Get("range").Number;
        heroID = (int)table.Get("heroID").Number;
        parts = GetParts(table);
        colors = GetColors(table);

        actions = GetActions(table);
    }

    private List<string> GetTags(Table table)
    {
        List<string> result = new List<string>();
        Table rawTags = table.Get("tags").Table;
        foreach (DynValue tag in rawTags.Values)
        {
            result.Add(tag.String);
        }
        return result;
    }

    private Dictionary<CARD_ATTRIBUTE, int> GetAttributes(Table table)
    {
        Dictionary<CARD_ATTRIBUTE, int> result = new Dictionary<CARD_ATTRIBUTE, int>();
        Table rawAttributes = table.Get("attributes").Table;
        foreach (DynValue attribute in rawAttributes.Keys)
        {
            // is it OK?
            result.Add((CARD_ATTRIBUTE)(int)attribute.Number, (int)rawAttributes.Get(attribute).Number);
        }
        return result;
    }

    private Dictionary<CARD_ACTION, Action<int, int>> GetActions(Table table)
    {
        Dictionary<CARD_ACTION, Action<int, int>> result = new Dictionary<CARD_ACTION, Action<int, int>>();
        Table rawActions = table.Get("actions").Table;
        foreach (DynValue action in rawActions.Keys)
        {
            Action<int, int> newAction = (int instanceID, int targetIID) => { LuaHelper.script.Call(rawActions.Get(action), instanceID, targetIID); };
            result.Add((CARD_ACTION)(int)action.Number, newAction);
        }
        return result;
    }

    private Dictionary<CARD_PART, string> GetParts(Table table)
    {
        Dictionary<CARD_PART, string> result = new Dictionary<CARD_PART, string>();
        Table rawParts = table.Get("parts").Table;
        foreach (DynValue part in rawParts.Keys)
        {
            // is it OK?
            result.Add((CARD_PART)(int)part.Number, rawParts.Get(part).String);
        }
        return result;
    }

    private Dictionary<CARD_COLOR, Color> GetColors(Table table)
    {
        Dictionary<CARD_COLOR, Color> result = new Dictionary<CARD_COLOR, Color>();
        Table rawColors = table.Get("colors").Table;
        foreach (DynValue color in rawColors.Keys)
        {
            Color newColor;
            if (ColorUtility.TryParseHtmlString(rawColors.Get(color).String, out newColor))
                result.Add((CARD_COLOR)(int)color.Number, newColor);
        }
        return result;
    }
}
