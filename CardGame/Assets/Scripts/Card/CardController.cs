﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public enum CARD_BEHAVIOR
{
    UNKNOWN,
    HAND,
    DECK,  
    DRAGABLE,
    SELECT
}



public class CardController : MonoBehaviour, IBeginDragHandler, 
    IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    public CardInstance cardInstance;
    public CARD_BEHAVIOR behavior;
    Vector2 offset = new Vector2();
    Vector2 homePos = new Vector2();

    public CardView cardView;

    RectTransform rectTransform;
    Canvas canvas;
    Animator animator;
    public bool drag = false;
    bool readyToPlay = false;
    bool selected = false;
    private int homeOrder;
    

    BattleDeckController deckController;
    TargetingController targetingController;


    private void Start()
    {
        canvas = GetComponent<Canvas>();
        rectTransform = GetComponent<RectTransform>();
        animator = GetComponent<Animator>();
        targetingController = FindObjectOfType<TargetingController>();
        deckController = FindObjectOfType<BattleDeckController>();
    }

    internal void Init(CardInstance cardToDraw, CARD_BEHAVIOR beh)
    {
        canvas = canvas ? canvas : GetComponent<Canvas>();
        rectTransform = rectTransform ? rectTransform : GetComponent<RectTransform>();
        behavior = beh;
        cardView = GetComponent<CardView>();
        cardInstance = cardToDraw;
        cardView.InitView();

        homeOrder = canvas.sortingOrder ;
        homePos = rectTransform.anchoredPosition;
    }

    public void SetOrder(int order)
    {
        canvas = canvas ? canvas : GetComponent<Canvas>();
        canvas.sortingLayerID = SortingLayer.NameToID("Cards");
        canvas.sortingOrder = order;
        homeOrder = canvas.sortingOrder;
        homePos = rectTransform.anchoredPosition;
    }

    private void Update()
    {
        if (drag)
        {
            if (Input.GetMouseButton(0))
            {

                switch (behavior)
                {
                    case CARD_BEHAVIOR.DRAGABLE:
                        transform.position = Vector2.Lerp((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition)
                                + offset, transform.position, 0.5f);
                        break;
                    case CARD_BEHAVIOR.HAND:
                        transform.position = Vector2.Lerp((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition)
                                + offset, transform.position, 0.5f);
                        rectTransform.anchoredPosition3D = new Vector3(rectTransform.anchoredPosition3D.x, rectTransform.anchoredPosition3D.y, 0);
                        /*
                        if (cardInstance.model.cardTarget == CARD_TARGET.ONE_ENEMY
                            && gameController.selectedMonster != null)
                        {
                            readyToPlay = true;
                            cardView.ReadyToPlay(true);
                        }

                        else if (cardInstance.model.cardTarget == CARD_TARGET.SELF && transform.position.y > 0)
                        {
                            readyToPlay = true;
                            cardView.ReadyToPlay(true);
                        }

                        else
                        {
                            cardView.ReadyToPlay(false);
                            readyToPlay = false;
                        }
                        */

                        break;
                    case CARD_BEHAVIOR.DECK:

                        break;
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                drag = false;
                deckController.drag = false;
                GetComponent<GraphicRaycaster>().enabled = true;
                switch (behavior)
                {
                    case CARD_BEHAVIOR.DRAGABLE:

                        break;
                    case CARD_BEHAVIOR.HAND:
                        targetingController.HideHighlights();
                        cardView.ReadyToPlay(false);

                        if (targetingController.currentTargets.Count > 0) 
                        {
                            PayCosts();

                            List<CharController> alive = targetingController.currentTargets.FindAll(x => !x.dead && !x.IsEmpty());

                            if (cardInstance.model.cardTarget == CARD_TARGET.RANDOM)
                            {
                                DoAction(CARD_ACTION.ON_PLAY, 0);
                            }
                            else
                            {
                                foreach (var t in alive)
                                {
                                    if (t.IsMonster())
                                    {
                                        DoAction(CARD_ACTION.ON_PLAY, t.monster.instanceID);
                                    }
                                    else
                                    {
                                        DoAction(CARD_ACTION.ON_PLAY, t.hero.instanceID);
                                    }
                                }
                            }
                            DoAction(CARD_ACTION.AFTER_PLAY, 0);
                            deckController.battleController.CheckWinCardPlayed();
                        }
                        else
                        {
                            canvas.sortingOrder = homeOrder;
                            rectTransform.anchoredPosition = homePos;
                        }
                        break;
                    case CARD_BEHAVIOR.DECK:

                        break;
                }
            }
        }

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //if (gameController.phase == PHASE.ENEMY) return;
        if (deckController.drag) return;

        switch (behavior)
        {
            case CARD_BEHAVIOR.DRAGABLE:
                offset = (Vector2)transform.position
            - (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);

                break;
            case CARD_BEHAVIOR.HAND:
                if (CheckActionCost())
                {
                    drag = true;
                    deckController.drag = true;
                    GetComponent<GraphicRaycaster>().enabled = false;
                    targetingController.HighlightPossibleTargets(cardInstance.model.cardTarget, cardInstance.model.range);
                    offset = (Vector2)transform.position - (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    rectTransform.anchoredPosition3D = new Vector3(rectTransform.anchoredPosition3D.x, rectTransform.anchoredPosition3D.y, 0);
                    canvas.sortingOrder = 100;
                }
                
                break;
            case CARD_BEHAVIOR.SELECT:
                break;
            case CARD_BEHAVIOR.DECK:
                break;
        }

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //if (gameController.phase == PHASE.ENEMY) return;

        switch (behavior)
        {
            case CARD_BEHAVIOR.SELECT:
                /*
                if (selected) {
                    selected = false;
                    cardView.ReadyToPlay(false);
                    gameController.RemoveFromSelected(this);
                }
                else if (gameController.selectedCards.Count < gameController.cardsToDiscard) {
                    selected = true;
                    gameController.AddToSelected(this);
                    cardView.ReadyToPlay(true);
                };
                */
                break;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (deckController.drag) return;

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (deckController.drag) return;
         
        if (!drag)
        {
            switch (behavior)
            {
                case CARD_BEHAVIOR.DRAGABLE:

                    break;
                case CARD_BEHAVIOR.HAND:

                    if (!CheckActionCost())
                    {
                        cardView.PointsHightlight(CheckActionCost());
                    }
                    canvas.sortingOrder = 100;
                    rectTransform.anchoredPosition = rectTransform.anchoredPosition + Vector2.up * 94;
                    break;

                case CARD_BEHAVIOR.SELECT:
                    homeOrder = canvas.sortingOrder;
                    homePos = (Vector2)transform.position;
                    break;
                case CARD_BEHAVIOR.DECK:

                    break;
            }
        }

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (deckController.drag) return;

        if (!drag)
        {
            switch (behavior)
            {
                case CARD_BEHAVIOR.DRAGABLE:

                    break;
                case CARD_BEHAVIOR.HAND:

                    cardView.PointsHightlight(true);
                    canvas.sortingOrder = homeOrder;
                    rectTransform.anchoredPosition = homePos;
                    break;
                case CARD_BEHAVIOR.SELECT:

                    break;
                case CARD_BEHAVIOR.DECK:

                    break;
            }
            
        }
        else
        {

        }
    }

    internal int GetModelID()
    {
        return cardInstance.GetModelID();
    }

    public void DoAction(CARD_ACTION act, int targetIID)
    {
        cardInstance.DoAction(act, targetIID);
    }


    public bool CheckActionCost()
    {
        var attr = cardInstance.attributes;
        return deckController.actions >= attr[CARD_ATTRIBUTE.ACTION_COST];
    }

    public void PayCosts()
    {
        var attr = cardInstance.attributes;
        deckController.PayAction(attr[CARD_ATTRIBUTE.ACTION_COST]);
    }

    internal void SetAttribute(CARD_ATTRIBUTE attr, int value)
    {
        cardInstance.attributes[attr] = value;
        cardView.UpdateView();
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
