MAIN_CONSTANTS = {
    AUTO_DISCARD = true,
    PERSISTENT_ARMOR = true
}

CARD_TARGET = {
    NO_ONE = 0,
    ALL = 1, ---  все враги (хз, работает ли)
    CLOSEST = 2, --- ближайший враг
    ANY = 3, ---  любой враг
    SELF = 4, --- на себя,
    RANDOM = 5, --- случайный враг
}

CARD_ATTRIBUTE = {
    UNKNOWN = 0,
    MANA_COST = 1, --- отображается на значке маны
    ACTION_COST = 2, --- отображается на значке экшена
    DAMAGE = 3, --- базовый урон карты
    ARMOR = 4, --- базовый армор карты
    MULTIPLIER = 5, --- в общем случае - сколько раз будет нанесён урон
    PENALTY = 6, --- всё, что можно вычесть
    COUNTER = 7, --- счётчик накладываемого статуса или множитель других действий, кроме атаки
    B_DAMAGE = 8, --- бонусный урон
    B_MULTIPLIER = 9, --- бонусный мультипликатор
    B_ARMOR = 10, --- бонусный армор
    BONUS = 11, --- всё, что можно добавить
}

CARD_ACTION = {
    UNKNOWN = 0,
    ON_DRAW = 1, --- TODO эффект карты, срабатывающий, когда она берётся из колоды
    ON_PLAY = 2, --- основной эффект, при разыгрывании
    ON_DISCARD = 3, --- эффект при сбросе карты
    ON_EXILE = 4, --- TODO эффект при изгнании карты
    ON_HEROES_SWAP = 5, --- эффект при перемещении героев
    ON_CHAR_DEATH = 6, --- эффект при смерти персонажа (любого, iID передаётся в событие)
    AFTER_PLAY = 7, --- когда карта отыграла

}

CARD_PART = {
    UNKNOWN = 0,
    ART = 1,
    ART_BACK = 2,
    PLATE = 3,
    PLAQUE = 4,
    FRAME = 5,
    ACTION_ART = 6,
    MANA_ART = 7,
}

CARD_COLOR = {
    UNKNOWN = 0,
    ART = 1,
    ART_BACK = 2,
    PLATE = 3,
    PLAQUE = 4,
    FRAME = 5,
    ACTION_ART = 6,
    MANA_ART = 7,
    ACTION_TEXT = 8,
    MANA_TEXT = 9,
    NAME_TEXT = 10,
    DESC_TEXT = 11,
}

CARD_RANGE = {
    MELEE = 0,
    RANGED = 1,
}

HERO_ATTRIBUTE = {
    UNKNOWN = 0,
    HEALTH = 1,
    MANA = 2,
    HAND = 3, --- макс размер руки
    DRAW = 4, --- число карт за ход
    ACTIONS = 5, --- экшен-поинты
    ARMOR = 6, --- армор на старте боя
}

MONSTER_ATTRIBUTE = {
    UNKNOWN = 0,
    HEALTH = 1,
    ARMOR = 2, --- армор на враге в начале боя
    DAMAGE = 3, --- добавляется к CARD_ATTRIBUTE.DAMAGE любой карты
    DEFENCE = 4 --- добавляется к CARD_ATTRIBUTE.DEFENCE любой карты
}

--- типы действий врагов
MONSTER_INTENTION = {
    UNKNOWN = 0,
    MELEE = 1, --- требует аттрибута DAMAGE, может иметь аттрибут MULTIPLIER, атакует ближнего героя
    RANGED = 2, --- требует аттрибута DAMAGE, может иметь аттрибут MULTIPLIER, атакует дальнего героя
    DEFEND_SELF = 3, --- требует аттрибута DEFENCE, вешает броню на себя
    WAIT = 4 --- не требует аттрибутов, пропускает ход
}

ACTION_ATTRIBUTE = {
    UNKNOWN = 0,
    DAMAGE = 1, --- базовый урон атаки
    MULTIPLIER = 2, --- множитель
    DEFENCE = 3 --- величина добавляемого армора при защите
}

STATUS_ATTRIBUTE = {
    UNKNOWN = 0,
    COUNTER = 1, --- как долго действует статус
---
    M_DAMAGE = 2, --- дополнительный наносимый урон на первой позиции
    M_ARMOR = 3, --- дополнительная получаемая броня на первой позиции
    M_INCOMING = 4, --- дополнительный входящий урон на первой позиции
---
    R_DAMAGE = 5, --- дополнительный наносимый урон на второй позиции (для монстров - на 2-3 позиции)
    R_ARMOR = 6, --- дополнительная получаемая броня на второй позиции (для монстров - на 2-3 позиции)
    R_INCOMING = 7, --- дополнительный входящий урон на второй позиции (для монстров - на 2-3 позиции)
}

STATUS_MODIFIER = {
    UNKNOWN = 0,

    M_DAMAGE = 1, --- множитель урона на первой позиции
    M_ARMOR = 2, --- множитель брони на первой позиции
    M_ACCURACY = 3, --- шанс попадания на первой позиции
    M_EVASION = 4, --- шанс увернуться на первой позиции
    M_INCOMING = 5, --- множитель входящего урона на первой позиции

    R_DAMAGE = 6, --- множитель урона на второй позиции (для монстров - на 2-3 позиции)
    R_ARMOR = 7, --- множитель брони на второй позиции (для монстров - на 2-3 позиции)
    R_ACCURACY = 8, --- шанс попадания на второй позиции (для монстров - на 2-3 позиции)
    R_EVASION = 9, --- шанс увернуться на второй позиции (для монстров - на 2-3 позиции)
    R_INCOMING = 10, --- множитель входящего урона на второй позиции (для монстров - на 2-3 позиции)
}

STATUS_TAG  = {
    STACKABLE = 1, --- при повторном добавлении статуса каунтеры складываются
    COUNTDOWN = 2, --- каунтер статуса уменьшается каждый ход
    PRECISION = 3, --- носитель всегда попадает
}

STATUS_ACTION = {
    UNKNOWN = 0,

    ON_ADD = 1, --- TODO
    ON_REMOVE = 2, --- TODO

    ON_TURN_START = 3, --- TODO
    ON_TURN_END = 4, --- TODO

    ON_CARD_DRAW = 5, --- TODO
    ON_CARD_PLAY = 6, --- TODO
    ON_CARD_DISCARD = 7, --- TODO
    ON_CARD_EXILE = 8, --- TODO

    ON_DAMAGE_RECIEVED = 9, --- TODO
    ON_ARMOR_ADD = 10, --- TODO
    ON_HEALTH_DAMAGE_RECEIVED = 11, --- TODO

    ON_CARD_CREATE = 12, --- TODO

}

--- для утилитарных внутренних функций
util = {
    --- сделать плоскую копию таблицы с оверрайдом.
    tableExtendedCopy = function(table, override)
        result = {}
        for key, value in ipairs(table)
        do
            if override[key] == nil
            then
                result[key] = value
            else
                result[key] = override[key]
            end
        end
        return result
    end,

    --- проверить, что все значения таблицы имеют разные значения по ключу id
    checkUniqueIds = function(table)
        seen = {}
        for key, value in ipairs(table)
        do
            id = value.id
            if seen[id] == nil
            then
                seen[id] = key
            else
                error(string.format("%s and %s has same id: %s", seen[id], key, id))
            end
        end
    end

}

API = {
    Monster = {
        ---@param iID number iID цели
        --- монстр, вызвавший этот метод, наносит урон в размере ACTION_ATTRIBUTE.DAMAGE ACTION_ATTRIBUTE.MULTIPLIER раз.
        AttackByAttributes = function(iID)
        end,

        ---@param iID number iID цели
        --- монстр, вызвавший этот метод, добавляет себе защиту в размере ACTION_ATTRIBUTE.DEFENCE.
        DefendByAttributes = function(iID)
        end
    },
    Card = {

        ---@param card number iID карты
        ---@param target number iID цели
        ---@param attrDMG number атрибут, используемый как источник урона
        ---@param attrMULT number атрибут, используемый как источник мультипликатора
        ---@param piercing boolean игнорировать броню цели
        --- нанести урон по target
        AttackByAttributes = function(card, target, attrDMG, attrMULT, piercing)
        end,

        ---@param card number iID карты
        ---@param target number iID цели
        ---@param attrARMR number атрибут, используемый как источник армора
        --- добавить броню на target
        DefendByAttributes = function(card, target, attrARMR)
        end,

        --- поменять героев местами
        SwapHeroes = function()
        end,

        ---@param target number iID цели
        --- оттолкнуть врага
        PushMonster = function(target)
        end,

        ---@param target number iID цели
        --- притянуть врага
        PullMonster = function(target)
        end,

        ---@param card number iID карты
        ---@param attrCNTR number атрибут, используемый как источник счётчика
        --- взять карты из стопки добора
        DrawCardsByCounter = function(card, attrCNTR)
        end,

        ---@param card number iID карты
        ---положить карту в стопку добора (не вызывая событие ON_CARD_DISCARD
        DiscardPlayed = function(card)
        end,

        ---@param card number iID карты
        ---@param target number iID монстра
        ---@param dmg number урон
        ---@param mult number мультипликатор
        ---@param piercing boolean игнорировать броню цели
        --- нанести урон, явно заданный аргументами
        AttackExplicit = function(card, target, dmg, mult, piercing)
        end,

        ---@param card number iID карты
        ---@param attr number атрибут
        ---@param value number значение
        --- присвоить значение атрибуту карты
        SetAttribute = function(card, attr, value)
        end,

        ---@param card number iID карты
        ---@param target number iID цели
        ---@param act number CARD_ACTION
        --- применить полностью указанный эффект карты
        ActionLink = function(card, target, act)
        end,

        ---@param card number iID карты
        ---@param target number iID цели
        ---@param armor number значение добавляемого армора
        --- добавить броню на target
        DefendExplicit = function(card, target, armor)
        end,

        ---@param card number iID карты
        ---@param target number iID цели
        ---@param statusID number ID статуса
        ---@param attr number аттрибут - источник каунтера
        --- добавить броню на target
        AddStatusByAttributes = function(card, target, statusID, attr)
        end,

        ---@param monster number iID монстра
        ---@param attr number MONSTER_ATTRIBUTE
        ---@param value number значение
        --- устанавливает атрибут моснтра
        SetMonsterAttribute = function(monster, attr, value)
        end,
    },
    Func = {
        ---@param card number iID карты
        ---@return boolean
        --- true, если на позиции карты стоит герой карты.
        AssertClass = function(card)
        end,

        ---@param card number iID карты
        ---@return number
        --- iID героя на позиции карты
        GetHeroByCardRange = function(card)
        end,

        ---@param hero number hero iID
        ---@param attr number HERO_ATTRIBUTE
        ---@return number значение атрибута
        --- возвращает атрибут героя
        GetHeroAttribute = function(hero, attr)
        end,

        ---@param card number iID карты
        ---@param attr number атрибут
        ---@return number значение атрибута
        --- возвращает значение атрибута МОДЕЛИ карты (т.е. значение по умолчанию)
        GetCardModelAttribute = function(card, attr)
        end,

        ---@param target number iID монстра
        ---@return number позиция
        --- возвращает позицию монстра по его iID
        GetMonsterPositionByIID = function(target) end,

        ---@param target number позиция монстра
        ---@return number позиция
        --- возвращает позицию монстра по его iID. 0, если такого монстра нет
        GetMonsterIIDByPosition = function(target) end,

        ---@param monster number iID монстра
        ---@param attr number MONSTER_ATTRIBUTE
        ---@return number значение атрибута
        --- возвращает атрибут монстра
        GetMonsterAttribute = function(monster, attr)
        end,

        ---@param range number позиция CARD_RANGE
        ---@return number
        --- iID героя на позиции, заданной явно
        GetHeroByRange = function(range)
        end,

        ---@param iID number iID сущности
        ---@return number
        --- ID модели сущности с данным IID (экземпляры героев, карт, монстров, статусов имеют уникальный iID
        --- который однозначно определяет экземпляр)
        GetModelIDByIID = function(iID) end
    }
}

--- спрайты частей карт
cardParts = {
    common = {
        [CARD_PART.ART] = "card/art",
        [CARD_PART.ART_BACK] = "card/artback",
        [CARD_PART.PLATE] = "card/plate_ellipse",
        [CARD_PART.PLAQUE] = "card/plaque_rounded",
        [CARD_PART.FRAME] = "card/frame",
        [CARD_PART.ACTION_ART] = "card/circle",
        [CARD_PART.MANA_ART] = "card/pentagon",
    },
    rare = {
        [CARD_PART.ART] = "card/art",
        [CARD_PART.ART_BACK] = "card/artback",
        [CARD_PART.PLATE] = "card/plate",
        [CARD_PART.PLAQUE] = "card/plaque",
        [CARD_PART.FRAME] = "card/frame_spikes",
        [CARD_PART.ACTION_ART] = "card/circle",
        [CARD_PART.MANA_ART] = "card/pentagon",
    },
}

--- цвета частей карт
cardColors = {
    commonDefenderMelee = {
        [CARD_COLOR.ART] = "#000000",
        [CARD_COLOR.ART_BACK] = "#999999",
        [CARD_COLOR.PLATE] = "#9a7b55",
        [CARD_COLOR.PLAQUE] = "#999999",
        [CARD_COLOR.FRAME] = "#999999",
        [CARD_COLOR.ACTION_ART] = "#999999",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },
    commonDefenderRange = {
        [CARD_COLOR.ART] = "#999999",
        [CARD_COLOR.ART_BACK] = "#000000",
        [CARD_COLOR.PLATE] = "#9a7b55",
        [CARD_COLOR.PLAQUE] = "#999999",
        [CARD_COLOR.FRAME] = "#999999",
        [CARD_COLOR.ACTION_ART] = "#999999",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },
    rareDefenderMelee = {
        [CARD_COLOR.ART] = "#000000",
        [CARD_COLOR.ART_BACK] = "#99ccff",
        [CARD_COLOR.PLATE] = "#9a7b55",
        [CARD_COLOR.PLAQUE] = "#99ccff",
        [CARD_COLOR.FRAME] = "#99ccff",
        [CARD_COLOR.ACTION_ART] = "#99ccff",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },
    rareDefenderRange = {
        [CARD_COLOR.ART] = "#99ccff",
        [CARD_COLOR.ART_BACK] = "#000000",
        [CARD_COLOR.PLATE] = "#9a7b55",
        [CARD_COLOR.PLAQUE] = "#99ccff",
        [CARD_COLOR.FRAME] = "#99ccff",
        [CARD_COLOR.ACTION_ART] = "#99ccff",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },

    commonRangerMelee = {
        [CARD_COLOR.ART] = "#000000",
        [CARD_COLOR.ART_BACK] = "#999999",
        [CARD_COLOR.PLATE] = "#688434",
        [CARD_COLOR.PLAQUE] = "#999999",
        [CARD_COLOR.FRAME] = "#999999",
        [CARD_COLOR.ACTION_ART] = "#999999",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },
    commonRangerRange = {
        [CARD_COLOR.ART] = "#999999",
        [CARD_COLOR.ART_BACK] = "#000000",
        [CARD_COLOR.PLATE] = "#688434",
        [CARD_COLOR.PLAQUE] = "#999999",
        [CARD_COLOR.FRAME] = "#999999",
        [CARD_COLOR.ACTION_ART] = "#999999",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },
    rareRangerMelee = {
        [CARD_COLOR.ART] = "#000000",
        [CARD_COLOR.ART_BACK] = "#99ccff",
        [CARD_COLOR.PLATE] = "#688434",
        [CARD_COLOR.PLAQUE] = "#99ccff",
        [CARD_COLOR.FRAME] = "#99ccff",
        [CARD_COLOR.ACTION_ART] = "#99ccff",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },
    rareRangerRange = {
        [CARD_COLOR.ART] = "#99ccff",
        [CARD_COLOR.ART_BACK] = "#000000",
        [CARD_COLOR.PLATE] = "#688434",
        [CARD_COLOR.PLAQUE] = "#99ccff",
        [CARD_COLOR.FRAME] = "#99ccff",
        [CARD_COLOR.ACTION_ART] = "#99ccff",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },

    common = {
        [CARD_COLOR.ART] = "#141020",
        [CARD_COLOR.ART_BACK] = "#C42C36",
        [CARD_COLOR.PLATE] = "#DBE0E7",
        [CARD_COLOR.PLAQUE] = "#67708B",
        [CARD_COLOR.FRAME] = "#A3ACBE",
        [CARD_COLOR.ACTION_ART] = "#F7AC37",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    },
    rare = {
        [CARD_COLOR.ART] = "#141020",
        [CARD_COLOR.ART_BACK] = "#C42C36",
        [CARD_COLOR.PLATE] = "#F0D2AF",
        [CARD_COLOR.PLAQUE] = "#E57028",
        [CARD_COLOR.FRAME] = "#C58158",
        [CARD_COLOR.ACTION_ART] = "#F7AC37",
        [CARD_COLOR.MANA_ART] = "#0E82CE",
        [CARD_COLOR.ACTION_TEXT] = "#212123",
        [CARD_COLOR.MANA_TEXT] = "#212123",
        [CARD_COLOR.NAME_TEXT] = "#212123",
        [CARD_COLOR.DESC_TEXT] = "#212123",
    }
}

--- декларация ID героев
heroIDs = {
    DEFENDER = 1,
    RANGER = 2
}

--- модели статусов
exportStatuses = {
    --- 1
    RestrictArmor = {
        id = 1,
        alias = "Restrict Armor",
        description = string.format("Restricts armor gain for this turn"),
        attributes = {
            [STATUS_ATTRIBUTE.COUNTER] = 1,
        },
        modifiers = {
            [STATUS_MODIFIER.M_ARMOR] = 0,
            [STATUS_MODIFIER.R_ARMOR] = 0,
        },
        actions ={
        },
        tags = {
            STATUS_TAG.COUNTDOWN
        },
        art = "card_art/BaseArmorBuff",
        color = "#890000",
    },

    --- 2
    RangedStrength = {
        id = 2,
        alias = "Ranged Strength",
        description = string.format("RANGED attacks deal M{%s}%% damage", STATUS_MODIFIER.R_DAMAGE),
        attributes = {
            [STATUS_ATTRIBUTE.COUNTER] = 1,
        },
        modifiers = {
            [STATUS_MODIFIER.R_DAMAGE] = 1.5
        },
        actions = {
        },
        tags = {
            STATUS_TAG.COUNTDOWN,
            STATUS_TAG.STACKABLE
        },
        art = "card_art/BaseAttackBuff",
        color = "#890000",
    },

    --- 3
    Precision = {
        id = 3,
        alias = "Precision",
        description = string.format("Character can't miss"),
        attributes = {
            [STATUS_ATTRIBUTE.COUNTER] = 1,
        },
        modifiers = {
            [STATUS_MODIFIER.R_ACCURACY] = 1000000,
            [STATUS_MODIFIER.M_ACCURACY] = 1000000,
        },
        actions = {
        },
        tags = {
            STATUS_TAG.COUNTDOWN,
            STATUS_TAG.STACKABLE
        },
        art = "card_art/Icon.1_07",
        color = "#890000",
    },

    --- 4
    Evasion = {
        id = 4,
        alias = "Evasion",
        description = string.format("Character can evade with M{%s}%% chance", STATUS_MODIFIER.R_EVASION),
        attributes = {
            [STATUS_ATTRIBUTE.COUNTER] = 1,
        },
        modifiers = {
            [STATUS_MODIFIER.R_EVASION] = 0.5,
            [STATUS_MODIFIER.M_EVASION] = 0.5,
        },
        actions = {
        },
        tags = {
            STATUS_TAG.COUNTDOWN,
            STATUS_TAG.STACKABLE
        },
        art = "card_art/Icons8_87",
        color = "#890000",
    },
}

--- модели карт
exportCards = {
    --- 1 ЗАЩИТНИК УДАР
    Strike = {
        id = 1,
        alias = "Strike",
        description = string.format("Deal {%s} damage. <b>{HERO}</b>: Deal {%s} damage instead",
                CARD_ATTRIBUTE.DAMAGE,
                CARD_ATTRIBUTE.B_DAMAGE
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 4,
            [CARD_ATTRIBUTE.MULTIPLIER] = 1,
            [CARD_ATTRIBUTE.B_DAMAGE] = 6,
        },
        cardTarget = CARD_TARGET.CLOSEST,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                if API.Func.AssertClass(card) then
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.B_DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                else
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.3_31" }
        ),
        colors = cardColors.commonDefenderMelee,
        tags = { }
    },

    --- 2 ЗАЩИТНИК ОБОРОНА
    Block = {
        id = 2,
        alias = "Block",
        description = string.format("Gain {%s} armor. <b>{HERO}</b>: Gain {%s} armor instead",
                CARD_ATTRIBUTE.ARMOR,
                CARD_ATTRIBUTE.B_ARMOR
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.ARMOR] = 4,
            [CARD_ATTRIBUTE.B_ARMOR] = 6,
        },
        cardTarget = CARD_TARGET.SELF,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                if API.Func.AssertClass(card) then
                    API.Card.DefendByAttributes(card, target, CARD_ATTRIBUTE.B_ARMOR)
                else
                    API.Card.DefendByAttributes(card, target, CARD_ATTRIBUTE.ARMOR)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.1_88_1" }
        ),
        colors = cardColors.commonDefenderMelee,
        tags = { }
    },

    --- 3 ЗАЩИТНИК БРОСОК
    Throw = {
        id = 3,
        alias = "Throw",
        description = string.format("Deal {%s} damage. <b>{HERO}</b>: Deal {%s} damage instead",
                CARD_ATTRIBUTE.DAMAGE,
                CARD_ATTRIBUTE.B_DAMAGE
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 4,
            [CARD_ATTRIBUTE.MULTIPLIER] = 1,
            [CARD_ATTRIBUTE.B_DAMAGE] = 6,
        },
        cardTarget = CARD_TARGET.ANY,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                if API.Func.AssertClass(card) then
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.B_DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                else
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.3_34" }
        ),
        colors = cardColors.commonDefenderRange,
        tags = { }
    },

    --- 4 ЗАЩИТНИК ЗАСЛОН
    TakeCover = {
        id = 4,
        alias = "Take Cover",
        description = string.format("Gain {%s} armor. <b>{HERO}</b>: Move forward",
                CARD_ATTRIBUTE.ARMOR
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.ARMOR] = 4,
        },
        cardTarget = CARD_TARGET.SELF,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.DefendByAttributes(card, target, CARD_ATTRIBUTE.ARMOR)
                if API.Func.AssertClass(card) then
                    API.Card.SwapHeroes()
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.1_04" }
        ),
        colors = cardColors.commonDefenderRange,
        tags = { }
    },

    --- 5 ЗАЩИТНИК ПИНОК
    Kick = {
        id = 5,
        alias = "Kick",
        description = string.format("Deal {%s} damage. <b>{HERO}</b>: Push enemy",
                CARD_ATTRIBUTE.DAMAGE
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 4,
            [CARD_ATTRIBUTE.MULTIPLIER] = 1
        },
        cardTarget = CARD_TARGET.CLOSEST,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                if API.Func.AssertClass(card) then
                    API.Card.PushMonster(target)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.1_29" }
        ),
        colors = cardColors.commonDefenderMelee,
        tags = { }
    },

    --- 6 ЗАЩИТНИК УДАР КОРПУСОМ
    BodyPunch = {
        id = 6,
        alias = "Body Punch",
        description = string.format("Deal {%s} damage. <b>{HERO}</b>: Gain {%s} armor",
                CARD_ATTRIBUTE.DAMAGE,
                CARD_ATTRIBUTE.B_ARMOR
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 3,
            [CARD_ATTRIBUTE.MULTIPLIER] = 1,
            [CARD_ATTRIBUTE.B_ARMOR] = 3,
        },
        cardTarget = CARD_TARGET.CLOSEST,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                if API.Func.AssertClass(card) then
                    hero = API.Func.GetHeroByCardRange(card)
                    API.Card.DefendByAttributes(card, hero, CARD_ATTRIBUTE.B_ARMOR)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.5_78" }
        ),
        colors = cardColors.commonDefenderMelee,
        tags = { }
    },

    --- 7 ЗАЩИТНИК ПРОВОКАЦИЯ
    Provoke = {
        id = 7,
        alias = "Provoke",
        description = string.format("Pull enemy. <b>{HERO}</b>: Gain {%s} armor",
                CARD_ATTRIBUTE.B_ARMOR
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.B_ARMOR] = 3,
        },
        cardTarget = CARD_TARGET.ANY,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.PullMonster(target)
                if API.Func.AssertClass(card) then
                    hero = API.Func.GetHeroByCardRange(card)
                    API.Card.DefendByAttributes(card, hero, CARD_ATTRIBUTE.B_ARMOR)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.2_93" }
        ),
        colors = cardColors.commonDefenderMelee,
        tags = { }
    },

    --- 8 ЗАЩИТНИК ЭКИПИРОВКА
    Equip = {
        id = 8,
        alias = "Equip",
        description = string.format("Draw {%s} card. <b>{HERO}</b>: Gain {%s} armor",
                CARD_ATTRIBUTE.COUNTER,
                CARD_ATTRIBUTE.B_ARMOR
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.COUNTER] = 1,
            [CARD_ATTRIBUTE.B_ARMOR] = 3,
        },
        cardTarget = CARD_TARGET.SELF,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.DrawCardsByCounter(card, CARD_ATTRIBUTE.COUNTER)
                if API.Func.AssertClass(card) then
                    API.Card.DefendByAttributes(card, target, CARD_ATTRIBUTE.B_ARMOR)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.2_67" }
        ),
        colors = cardColors.commonDefenderMelee,
        tags = { }
    },

    --- 9 ЗАЩИТНИК УДАР ЩИТОМ
    ShieldBash = {
        id = 9,
        alias = "Shield Bash",
        description = string.format("Deal damage equal to armor. <b>{HERO}</b>: Action cost lower by {%s}",
                CARD_ATTRIBUTE.PENALTY
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 2,
            [CARD_ATTRIBUTE.PENALTY] = 1
        },
        cardTarget = CARD_TARGET.CLOSEST,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                hero = API.Func.GetHeroByCardRange(card)
                armor = API.Func.GetHeroAttribute(hero, HERO_ATTRIBUTE.ARMOR)
                API.Card.AttackExplicit(card, target, armor, 1, false)
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
            [CARD_ACTION.ON_DRAW] = function(card, _formal)
                cost = API.Func.GetCardModelAttribute(card, CARD_ATTRIBUTE.ACTION_COST)
                if API.Func.AssertClass(card) then
                    discount = API.Func.GetCardModelAttribute(card, CARD_ATTRIBUTE.PENALTY)
                    cost = cost - discount
                end
                API.Card.SetAttribute(card, CARD_ATTRIBUTE.ACTION_COST, cost)
            end,
            [CARD_ACTION.ON_HEROES_SWAP] = function(card, _formal)
                API.Card.ActionLink(card, _formal, CARD_ACTION.ON_DRAW)
            end,
            [CARD_ACTION.ON_CHAR_DEATH] = function(card, dead)
                API.Card.ActionLink(card, dead, CARD_ACTION.ON_DRAW)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icons8_83" }
        ),
        colors = cardColors.rareDefenderMelee,
        tags = { }
    },

    --- 10 ЗАЩИТНИК ПОСЛЕДНИЙ РУБЕЖ
    LastFrontier = {
        id = 10,
        alias = "Last Frontier",
        description = string.format("Gain {%s} armor. Gain <b>Restrict armor</b>. <b>{HERO}</b>: Gain {%s} more armor ",
                CARD_ATTRIBUTE.ARMOR,
                CARD_ATTRIBUTE.BONUS
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 2,
            [CARD_ATTRIBUTE.ARMOR] = 10,
            [CARD_ATTRIBUTE.BONUS] = 10,
            [CARD_ATTRIBUTE.COUNTER] = 1
        },
        cardTarget = CARD_TARGET.SELF,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.DEFENDER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                if API.Func.AssertClass(card) then
                    armor = API.Func.GetCardModelAttribute(card, CARD_ATTRIBUTE.ARMOR)
                    bonus = API.Func.GetCardModelAttribute(card, CARD_ATTRIBUTE.BONUS)
                    armor = armor + bonus;
                    API.Card.DefendExplicit(card, target, armor)
                else
                    API.Card.DefendByAttributes(card, target, CARD_ATTRIBUTE.ARMOR)
                end
                API.Card.AddStatusByAttributes(card, target, exportStatuses.RestrictArmor.id, CARD_ATTRIBUTE.COUNTER)
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.1_51" }
        ),
        colors = cardColors.rareDefenderMelee,
        tags = { }
    },

    --- 11 РЕЙНДЖЕР ВЫСТРЕЛ
    Shot = {
        id = 11,
        alias = "Shot",
        description = string.format("Deal {%s} damage. <b>{HERO}</b>: Deal {%s} damage instead",
                CARD_ATTRIBUTE.DAMAGE,
                CARD_ATTRIBUTE.B_DAMAGE
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 4,
            [CARD_ATTRIBUTE.MULTIPLIER] = 1,
            [CARD_ATTRIBUTE.B_DAMAGE] = 6,
        },
        cardTarget = CARD_TARGET.ANY,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                if API.Func.AssertClass(card) then
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.B_DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                else
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.3_33" }
        ),
        colors = cardColors.commonRangerRange,
        tags = { }
    },

    --- 12 РЕЙНДЖЕР УКРЫТИЕ
    Cover = {
        id = 12,
        alias = "Cover",
        description = string.format("Gain {%s} armor. <b>{HERO}</b>: Gain {%s} armor instead",
                CARD_ATTRIBUTE.ARMOR,
                CARD_ATTRIBUTE.B_ARMOR
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.ARMOR] = 4,
            [CARD_ATTRIBUTE.B_ARMOR] = 6,
        },
        cardTarget = CARD_TARGET.SELF,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                if API.Func.AssertClass(card) then
                    API.Card.DefendByAttributes(card, target, CARD_ATTRIBUTE.B_ARMOR)
                else
                    API.Card.DefendByAttributes(card, target, CARD_ATTRIBUTE.ARMOR)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.5_58" }
        ),
        colors = cardColors.commonRangerRange,
        tags = { }
    },

    --- 13 РЕЙНДЖЕР ПОРЕЗ
    Cut = {
        id = 13,
        alias = "Cut",
        description = string.format("Deal {%s} damage. <b>{HERO}</b>: Deal {%s} damage instead",
                CARD_ATTRIBUTE.DAMAGE,
                CARD_ATTRIBUTE.B_DAMAGE
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.MULTIPLIER] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 4,
            [CARD_ATTRIBUTE.B_DAMAGE] = 6,
        },
        cardTarget = CARD_TARGET.CLOSEST,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                if API.Func.AssertClass(card) then
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.B_DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                else
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icons8_21" }
        ),
        colors = cardColors.commonRangerMelee,
        tags = { }
    },

    --- 14 РЕЙНДЖЕР ОТСТУПЛЕНИЕ
    Retreat = {
        id = 14,
        alias = "Retreat",
        description = string.format("Gain {%s} armor. <b>{HERO}</b>: Move backward",
                CARD_ATTRIBUTE.ARMOR
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.ARMOR] = 4
        },
        cardTarget = CARD_TARGET.SELF,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.DefendByAttributes(card, target, CARD_ATTRIBUTE.ARMOR)
                if API.Func.AssertClass(card) then
                    API.Card.SwapHeroes()
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.1_95" }
        ),
        colors = cardColors.commonRangerMelee,
        tags = { }
    },

    --- 15 РЕЙНДЖЕР БЕГЛЫЙ ОГОНЬ
    QuickFire = {
        id = 15,
        alias = "Quick Fire",
        description = string.format("Deal {%s} damage to random enemy {%s} times. <b>{HERO}</b>: Move backwards",
                CARD_ATTRIBUTE.DAMAGE,
                CARD_ATTRIBUTE.MULTIPLIER
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 2,
            [CARD_ATTRIBUTE.MULTIPLIER] = 3
        },
        cardTarget = CARD_TARGET.RANDOM,
        range = CARD_RANGE.MELEE,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                if API.Func.AssertClass(card) then
                    API.Card.SwapHeroes()
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.1_43" }
        ),
        colors = cardColors.commonRangerMelee,
        tags = { }
    },

    --- 16 РЕЙНДЖЕР ВЫСТРЕЛ НАВЫЛЕТ
    ShotThrough = {
        id = 16,
        alias = "Shot Through",
        description = string.format("Deal {%s} damage. <b>{HERO}</b>: Deal {%s} damage to enemy behind the target",
                CARD_ATTRIBUTE.DAMAGE,
                CARD_ATTRIBUTE.B_DAMAGE
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 3,
            [CARD_ATTRIBUTE.B_DAMAGE] = 3,
            [CARD_ATTRIBUTE.MULTIPLIER] = 1
        },
        cardTarget = CARD_TARGET.ANY,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                if API.Func.AssertClass(card) then
                    pos = API.Func.GetMonsterPositionByIID(target) + 1
                    targetBehind = API.Func.GetMonsterIIDByPosition(pos)
                    if targetBehind > 0 then
                        API.Card.AttackByAttributes(card, targetBehind, CARD_ATTRIBUTE.B_DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                    end
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.2_40" }
        ),
        colors = cardColors.commonRangerRange,
        tags = { }
    },

    --- 17 РЕЙНДЖЕР ТОЧНОЕ ПОПАДАНИЕ
    PerfectHit = {
        id = 17,
        alias = "Perfect Hit",
        description = string.format("Deal {%s} damage. <b>{HERO}</b>: Ignore armor",
                CARD_ATTRIBUTE.DAMAGE
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.DAMAGE] = 3,
            [CARD_ATTRIBUTE.MULTIPLIER] = 1
        },
        cardTarget = CARD_TARGET.ANY,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                if API.Func.AssertClass(card) then
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, true)
                else
                    API.Card.AttackByAttributes(card, target, CARD_ATTRIBUTE.DAMAGE, CARD_ATTRIBUTE.MULTIPLIER, false)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.1_16" }
        ),
        colors = cardColors.commonRangerRange,
        tags = { }
    },

    --- 18 РЕЙНДЖЕР НА МУШКЕ
    TakeAim = {
        id = 18,
        alias = "Take Aim",
        description = string.format("Gain {%s} <b>Ranged Strength</b>. <b>{HERO}</b>: Also gain {%s} <b>Precision</b>",
                CARD_ATTRIBUTE.COUNTER,
                CARD_ATTRIBUTE.BONUS
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.COUNTER] = 1,
            [CARD_ATTRIBUTE.BONUS] = 1,
        },
        cardTarget = CARD_TARGET.SELF,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                hero =  API.Func.GetHeroByCardRange(card)
                API.Card.AddStatusByAttributes(card, hero, exportStatuses.RangedStrength.id, CARD_ATTRIBUTE.COUNTER)
                if API.Func.AssertClass(card) then
                    API.Card.AddStatusByAttributes(card, hero, exportStatuses.Precision.id, CARD_ATTRIBUTE.BONUS)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.1_07" }
        ),
        colors = cardColors.commonRangerRange,
        tags = { }
    },

    --- 19 РЕЙНДЖЕР ГОДЫ ПОДГОТОВКИ
    YearsOfPractice = {
        id = 19,
        alias = "Years Of Practice",
        description = string.format("Gain {%s} <b>Evasion</b>. <b>{HERO}</b>: Gain {%s} <b>Evasion</b> instead",
                CARD_ATTRIBUTE.COUNTER,
                CARD_ATTRIBUTE.BONUS
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 1,
            [CARD_ATTRIBUTE.COUNTER] = 1,
            [CARD_ATTRIBUTE.BONUS] = 2,
        },
        cardTarget = CARD_TARGET.SELF,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                hero =  API.Func.GetHeroByCardRange(card)
                if API.Func.AssertClass(card) then
                    API.Card.AddStatusByAttributes(card, hero, exportStatuses.Evasion.id, CARD_ATTRIBUTE.BONUS)
                else
                    API.Card.AddStatusByAttributes(card, hero, exportStatuses.Evasion.id, CARD_ATTRIBUTE.COUNTER)
                end
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icons8_87" }
        ),
        colors = cardColors.rareRangerRange,
        tags = { }
    },

    --- 20 РЕЙНДЖЕР ТАКТИЧЕСКОЕ ПРЕВОСХОДСТВО
    TacticalSuperiority = {
        id = 20,
        alias = "Tactical Superiority",
        description = string.format("Destroy enemy armor. <b>{HERO}</b>: Action cost lower by {%s}",
                CARD_ATTRIBUTE.PENALTY
        ),
        attributes = {
            [CARD_ATTRIBUTE.ACTION_COST] = 3,
            [CARD_ATTRIBUTE.PENALTY] = 1
        },
        cardTarget = CARD_TARGET.ANY,
        range = CARD_RANGE.RANGED,
        heroID = heroIDs.RANGER,
        actions = {
            [CARD_ACTION.ON_PLAY] = function(card, target)
                API.Card.SetMonsterAttribute(target, MONSTER_ATTRIBUTE.ARMOR, 0)
            end,
            [CARD_ACTION.AFTER_PLAY] = function(card, _formal)
                API.Card.DiscardPlayed(card)
            end,
            [CARD_ACTION.ON_DRAW] = function(card, _formal)
                cost = API.Func.GetCardModelAttribute(card, CARD_ATTRIBUTE.ACTION_COST)
                if API.Func.AssertClass(card) then
                    discount = API.Func.GetCardModelAttribute(card, CARD_ATTRIBUTE.PENALTY)
                    cost = cost - discount
                end
                API.Card.SetAttribute(card, CARD_ATTRIBUTE.ACTION_COST, cost)
            end,
            [CARD_ACTION.ON_HEROES_SWAP] = function(card, _formal)
                API.Card.ActionLink(card, _formal, CARD_ACTION.ON_DRAW)
            end,
            [CARD_ACTION.ON_CHAR_DEATH] = function(card, dead)
                API.Card.ActionLink(card, dead, CARD_ACTION.ON_DRAW)
            end,
        },
        parts = util.tableExtendedCopy(cardParts.common, {
            [CARD_PART.ART] = "card_art/Icon.5_37" }
        ),
        colors = cardColors.rareRangerRange,
        tags = { }
    },
}

--- иконки намерений
--- без кода не пополнять
exportIntentionsIcons = {
    [MONSTER_INTENTION.MELEE] = "card_art/Icon.2_43",
    [MONSTER_INTENTION.RANGED] = "card_art/Icon.1_16",
    [MONSTER_INTENTION.DEFEND_SELF] = "card_art/Icon.1_88",
    [MONSTER_INTENTION.WAIT] = "card_art/Icon.2_98",
}

--- атрибуты игрока
exportPlayerDefault = {
    attributes = {
        [HERO_ATTRIBUTE.HEALTH] = 50,
        [HERO_ATTRIBUTE.DRAW] = 4,
        [HERO_ATTRIBUTE.HAND] = 10,
        [HERO_ATTRIBUTE.ACTIONS] = 3,
        [HERO_ATTRIBUTE.MANA] = 0,
        [HERO_ATTRIBUTE.ARMOR] = 0,
    },
}

--- модели героев
exportHeroes = {
    DEFENDER = {
        id = heroIDs.DEFENDER,
        alias = "Defender",
        description = "warrior_desc",
        art = "characters/arnold",
        attributes = {
            [HERO_ATTRIBUTE.HEALTH] = 10,
            [HERO_ATTRIBUTE.DRAW] = 0,
            [HERO_ATTRIBUTE.HAND] = 0,
            [HERO_ATTRIBUTE.ACTIONS] = 0,
            [HERO_ATTRIBUTE.MANA] = 0,
            [HERO_ATTRIBUTE.ARMOR] = 10,
        },
        tags = { "WARRIOR" },
        startingDeck = {
            --- exportCards.Strike.id,
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20

        },
        startingStatuses = {
            --- [2] = 1
        }
    },
    RANGER = {
        id = heroIDs.RANGER,
        alias = "Ranger",
        description = "warrior_desc",
        art = "characters/girl",
        attributes = {
            [HERO_ATTRIBUTE.HEALTH] = -10,
            [HERO_ATTRIBUTE.DRAW] = 1,
            [HERO_ATTRIBUTE.HAND] = 0,
            [HERO_ATTRIBUTE.ACTIONS] = 0,
            [HERO_ATTRIBUTE.MANA] = 0,
            [HERO_ATTRIBUTE.ARMOR] = 0,
        },
        tags = { "RANGER" },
        startingDeck = {



        },
        startingStatuses = {
        }
    }

}

--- WORK IN PROGRESS
cor = nil
corResume = function()
    print("test2")
    coroutine.resume(cor)
    print("test3")
end

--- модели монстров
exportMonsters = {
    SPIDERMAN = {
        id = 1,
        alias = "SPIDERMAN",
        description = "",
        art = "characters/spiderman",
        intentionHeight = -35,
        attributes = {
            [MONSTER_ATTRIBUTE.HEALTH] = 15,
            [MONSTER_ATTRIBUTE.ARMOR] = 0,
            [MONSTER_ATTRIBUTE.DAMAGE] = 0,
            [MONSTER_ATTRIBUTE.DEFENCE] = 0,
        },
        actions = {
            ATTACK = {
                intention = MONSTER_INTENTION.MELEE,
                attributes = {
                    [ACTION_ATTRIBUTE.DAMAGE] = 8,
                },
                action = function(iID)
                    API.Monster.AttackByAttributes(iID);
                end
            },
            RANGE_ATTACK = {
                intention = MONSTER_INTENTION.RANGED,
                attributes = {
                    [ACTION_ATTRIBUTE.DAMAGE] = 3,
                    [ACTION_ATTRIBUTE.MULTIPLIER] = 4,
                },
                action = function(iID)
                    API.Monster.AttackByAttributes(iID);
                end
            },
            DEFENCE = {
                intention = MONSTER_INTENTION.DEFEND_SELF,
                attributes = {
                    [ACTION_ATTRIBUTE.DEFENCE] = 10,
                },
                action = function(iID)
                    API.Monster.DefendByAttributes(iID)
                end,
            }
        },
        tactics = function(turn, lastAttack)
            --seq = {
            --    "HEAVY_ATTACK",
            --    "DEFENCE",
            --    "ATTACK"
            --}
            --return seq[(turn % #seq) + 1]
            rnd = math.random(1, 3)
            actionsTable = {
                [1] = function()
                    return "ATTACK"
                end,
                [2] = function()
                    return "RANGE_ATTACK"
                end,
                [3] = function()
                    return "RANGE_ATTACK"
                end,
                [4] = function()
                    return "DEFENCE"
                end,
                [2] = function()
                    return "DEFENCE"
                end,
            }
            return actionsTable[rnd]()
        end,
        tags = {},
        startingStatuses = {},

    },
    SERGEY = {
        id = 2,
        alias = "SERGEY",
        description = "",
        art = "characters/sergey",
        intentionHeight = 82,
        attributes = {
            [MONSTER_ATTRIBUTE.HEALTH] = 25,
            [MONSTER_ATTRIBUTE.ARMOR] = 0,
            [MONSTER_ATTRIBUTE.DAMAGE] = 0,
            [MONSTER_ATTRIBUTE.DEFENCE] = 0,
        },
        actions = {
            ATTACK = {
                intention = MONSTER_INTENTION.MELEE,
                attributes = {
                    [ACTION_ATTRIBUTE.DAMAGE] = 10,
                },
                action = function(iID)
                    API.Monster.AttackByAttributes(iID);
                end
            },
            DEFENCE = {
                intention = MONSTER_INTENTION.DEFEND_SELF,
                attributes = {
                    [ACTION_ATTRIBUTE.DEFENCE] = 15,
                },
                action = function(iID)
                    API.Monster.DefendByAttributes(iID);
                end,
            },
            SWAP = {
                intention = MONSTER_INTENTION.UNKNOWN,
                attributes = {
                },
                action = function(iID)
                    API.Card.SwapHeroes()
                end,
            }
        },
        tactics = function(turn, lastAttack)
            rnd = math.random(1, 3)
            actionsTable = {
                [1] = function()
                    return "ATTACK"
                end,
                [2] = function()
                    return "DEFENCE"
                end,
                [3] = function()
                    coin = math.random(0, 1)
                    if (API.Func.GetHeroByRange(CARD_RANGE.MELEE) == exportHeroes.DEFENDER.id and coin == 0)
                    then
                        return "SWAP"
                    else
                        return "ATTACK"
                    end
                end,
            }
            return actionsTable[rnd]()
        end,
        tags = {},
        startingStatuses = {},
    }
}

--- модели комнат
exportBattleRooms = {
    TEST_ROOM_1 = {
        id = 1,
        backArt = "room_art/bricks",
        monsters = { --- не более трёх монстров
                     exportMonsters.SERGEY.id,
                     exportMonsters.SPIDERMAN.id,
                     exportMonsters.SPIDERMAN.id,
        }
    },
    TEST_ROOM_2 = {
        id = 2,
        backArt = "room_art/bricks",
        monsters = { --- не более трёх монстров
                     exportMonsters.SERGEY.id,
                     exportMonsters.SERGEY.id,
        }
    }
}

--- приключения - последовательности комнат
exportAdventures = {
    ADV_1 = {
        id = 1,
        rooms = {
            exportBattleRooms.TEST_ROOM_1.id,
            exportBattleRooms.TEST_ROOM_2.id
        }
    },
}


--- отдаёт клиенту приключение
function exportGetAdventure()
    return adventures[1];
end;

--- GLOBAL CHECKS
util.checkUniqueIds(exportCards);
util.checkUniqueIds(exportHeroes);
util.checkUniqueIds(exportMonsters);
util.checkUniqueIds(exportBattleRooms);
util.checkUniqueIds(exportStatuses);
print("______")
print(exportCards.Strike.actions[CARD_ACTION.ON_PLAY]())
