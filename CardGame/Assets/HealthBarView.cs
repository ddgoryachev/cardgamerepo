﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class HealthBarView : MonoBehaviour
{
    CharController charController;

    [SerializeField] Slider slider;
    [SerializeField] TextMeshProUGUI hpText;
    [SerializeField] Image armor;
    [SerializeField] TextMeshProUGUI armorText;

    private void Start()
    {
        CheckAndGetCharController();
    }

    internal void UpdateView()
    {
        List<int> hp = charController.GetHP();
        int armr = charController.GetArmor();

        slider.value = (float)hp[0] / hp[1];
        hpText.text = string.Format("{0}/{1}", hp[0], hp[1]);
        armor.enabled = armr > 0;
        armorText.text = armr > 0 ? armr.ToString() : "";
    }

    void CheckAndGetCharController()
    {
        if (!charController) charController = GetComponentInParent<CharController>();
    }
}
