﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusBarController : MonoBehaviour
{
    [SerializeField] CharController charController;
    [SerializeField] List<StatusController> statusControllers;

    public void UpdateAll()
    {
        List<StatusInstance> statuses = charController.GetStatuses();

        for (int i = 0; i<statusControllers.Count; i++)
        {
            if (i < statuses.Count) statusControllers[i].UpdateView(statuses[i]);
            else statusControllers[i].Clear();
        }

    }
}
