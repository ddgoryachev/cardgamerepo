﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour
{
    [SerializeField] CharView charView;
    [SerializeField] TargetZoneController targetZone;
    BattleController battleController;
    GameController gameController;

    public HeroInstance hero;
    public MonsterInstance monster;
    public bool dead;

    private void Start()
    {
        gameController = FindObjectOfType<GameController>();
        battleController = FindObjectOfType<BattleController>();
    }

    public void Init()
    {
        hero = null;
        monster = null;
        charView.UpdateAll();
    }

    public void Init(HeroInstance heroInstance)
    {
        dead = heroInstance.dead;
        hero = heroInstance;
        hero.ReInit();
        charView.UpdateAll();
    }

    public void Init(MonsterInstance monsterInstance)
    {
        if (monsterInstance == null) Init();
        dead = monsterInstance.dead;
        monster = monsterInstance;
        charView.UpdateAll();
    }

    internal int GetArmor()
    {
        return hero != null ? hero.GetArmor() : monster.GetArmor();
    }

    internal void SetArmor(int arm)
    {
        if (hero != null) hero.SetArmor(arm);
        else monster.SetArmor(arm);
    }

    internal void AddArmor(int arm, bool fromCard)
    {
        if (hero != null) {
            int range = battleController.heroes.FindIndex(x => x == hero);
            hero.AddArmor(arm, fromCard, range);
        } 
        else monster.AddArmor(arm);

        charView.UpdateAll();
    }

    internal List<int> GetHP()
    {
        return hero != null ? hero.GetHP() : monster.GetHP();
    }

    internal void SetHP(int hp)
    {
        if (hero != null) hero.SetHP(hp);
        else monster.SetHP(hp);
    }

    internal int GetIID()
    {
        if (IsEmpty()) return 0;
        return (hero != null) ? hero.instanceID : monster.instanceID;
    }

    internal void DoAction()
    {
        Debug.Log(monster.instanceID + ": " + monster.preparedAction);

        monster.GetPreparedAction().action(GetIID());
        monster.ChooseNextAction(battleController.Turn);

        charView.UpdateAll();
    }

    public bool IsMonster()
    {
        return monster != null;
    }

    public bool IsEmpty()
    {
        return monster == null && hero == null;
    }

    private void Die()
    {
        StopAllCoroutines();

        dead = true;
        if (IsMonster()) {
            monster.dead = true;
            battleController.statsController.monstersController.CheckWin(this);
        }
        else hero.dead = true;

        charView.UpdateAll();
        battleController.handController.DoActions(CARD_ACTION.ON_CHAR_DEATH, this);
    }

    public int GetStatusAttributesSumm(STATUS_ATTRIBUTE attr)
    {
        if (IsMonster()) return monster.GetStatusAttributesSumm(attr);
        else if (hero != null) return hero.GetStatusAttributesSumm(attr);
        return 0;
    }

    public float GetStatusModifiersProduct(STATUS_MODIFIER mod)
    {
        if (IsMonster()) return monster.GetStatusModifiersProduct(mod);
        else if (hero != null) return hero.GetStatusModifiersProduct(mod);
        return 1;
    }

    public float GetFullEvasionFail(int position)
    {
        if (IsMonster()) return monster.GetFullEvasionFail(position);
        else if (hero != null) return hero.GetFullEvasionFail(position);
        return 1;
    }

    public void ReceiveDamage(int damage, bool piercing, CharController attacker)
    {
        float chanceToHit = API.NoAPI.CalcHitChance(attacker, this);


        if (chanceToHit > UnityEngine.Random.value) {
            Debug.Log("HIT! Chance to hit was " + chanceToHit);
            if (piercing)
            {
                SetHP(Mathf.Max(GetHP()[0] - damage, 0));
            }
            else
            {
                int armorLeft = GetArmor() - damage;
                SetArmor(Mathf.Max(0, armorLeft));
                SetHP(Mathf.Max(GetHP()[0] + Mathf.Min(armorLeft, 0), 0));
            }


            charView.UpdateAll();

            if (GetHP()[0] <= 0) Die();

        }
        else
        {
            Debug.Log("MISS! Chance to hit was " + chanceToHit);
        }
        
    }

    public void ReceiveDamageMultiple(int damage, int mult, bool piercing, CharController attacker)
    {
        for (int i = 0; i < mult; i++)
        {
            if (dead) break;

            

            ReceiveDamage(damage, piercing, attacker);
        }
    }

    // MONSTER

    internal void AttackByAttributes()
    {
        MonsterAction action = monster.GetPreparedAction();

        int mult = action.attributes.ContainsKey(ACTION_ATTRIBUTE.MULTIPLIER)
            ? action.attributes[ACTION_ATTRIBUTE.MULTIPLIER] : 1;

        CharController target;

        switch (action.intention)
        {
            case MONSTER_INTENTION.MELEE:
                target = battleController.statsController.heroesController.charControllers[0];
                break;
            case MONSTER_INTENTION.RANGED:
                target = battleController.statsController.heroesController.charControllers[1];
                break;
            default:
                throw new Exception($"AttackByAttributes can't be used with intention {action.intention}, check monster {monster.model.name}, action {monster.preparedAction}");
        }

        int damage = monster.GetPreparedActionDamage(target);

        

        target.ReceiveDamageMultiple(damage, mult, false, this);
    }


    internal void DefendByAttributes()
    {
        MonsterAction action = monster.GetPreparedAction();

        CharController target;

        switch (action.intention)
        {
            case MONSTER_INTENTION.DEFEND_SELF:
                target = this;
                break;
            default:
                throw new Exception($"DefendByAttributes can't be used with intention {action.intention}, check monster {monster.model.name}, action {monster.preparedAction}");
        }

        target.AddArmor(monster.GetPreparedAction().attributes[ACTION_ATTRIBUTE.DEFENCE], false);

    }

    public int GetPos()
    {
        return API.NoAPI.GetCharPosition(this);
    }

    public void AddStatus(int id, int counter)
    {
        if (IsEmpty() || dead) return;
        else if (IsMonster()) monster.AddStatus(id, counter);
        else hero.AddStatus(id, counter);
        charView.UpdateAll();
    }


    internal List<StatusInstance> GetStatuses()
    {
        if (dead || IsEmpty())
        {
            return new List<StatusInstance>();
        }
        else if (IsMonster()) return monster.statusInstances;
        else return hero.statusInstances;
    }

    public void StatusCountdown()
    {
        if (hero != null) hero.StatusCountdown();
        if (monster != null) monster.StatusCountdown();
        charView.UpdateAll();
    }

    internal void SetAttribute(MONSTER_ATTRIBUTE attr, int value)
    {
        monster.SetAttribute(attr, value);
        charView.UpdateAll();
    }

    public int GetModelID()
    {
        if (hero != null) return hero.GetModelID();
        else if (monster != null) return monster.GetModelID();
        else return -1;
    }

    public StatusInstance GetStatusByIID(int iID)
    {
        List<StatusInstance> statuses = new List<StatusInstance>();
        if (hero != null) {
            statuses = hero.statusInstances;
        }
        else if (monster != null)
        {
            statuses = monster.statusInstances;
        }
        return statuses.Find(x => x.instanceID == iID);
    }
}
