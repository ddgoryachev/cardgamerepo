﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ActionView : MonoBehaviour
{
    BattleDeckController battleDeckController;
    [SerializeField] TextMeshProUGUI text;

    void Start()
    {
        battleDeckController = FindObjectOfType<BattleDeckController>();
    }

    public void UpdateView()
    {
        text.text = $"{battleDeckController.actions}/{battleDeckController.maxActions}";
    }
}
