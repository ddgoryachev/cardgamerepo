﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TargetingController : MonoBehaviour
{
    StatsController statsController;

    [SerializeField] List<TargetController> selfTargets;
    [SerializeField] List<TargetController> heroes;
    [SerializeField] List<TargetController> monsters;
    [SerializeField] TargetController allMonsters;
    [SerializeField] Image fader;
    public List<CharController> currentTargets;

    private void Start()
    {
        statsController = FindObjectOfType<StatsController>();
        fader.gameObject.SetActive(false);
    }

    public void HighlightPossibleTargets(CARD_TARGET target, int range) {

        fader.gameObject.SetActive(true);
        heroes[range].Show();
        switch (target)
        {
            case CARD_TARGET.RANDOM:
            case CARD_TARGET.ALL:
                allMonsters.Show();
                heroes[range].SetFocus(true);
                break;
            case CARD_TARGET.CLOSEST:
                int index = statsController.monstersController.GetClosest();
                monsters[index].Show();
                break;
            case CARD_TARGET.ANY:
                for (int i = 0; i < 3; i++)
                {
                    CharController monster = statsController.monstersController.monsterControllers[i];
                    if (monster.dead || monster.IsEmpty()) { }
                    else {
                        monsters[i].Show();
                    }
                }
                break;
            case CARD_TARGET.SELF:
                selfTargets[range].Show();
                break;

        }

    }

    internal void RemoveTarget(TargetController targetController)
    {
        if (currentTargets == targetController.charControllers) currentTargets = new List<CharController>();
    }

    internal void SetTarget(TargetController targetController)
    {
        currentTargets = targetController.charControllers;
    }

    public void HideHighlights()
    {
        fader.gameObject.SetActive(false);
        allMonsters.Hide();

        selfTargets.ForEach(x => x.Hide());
        monsters.ForEach(x => x.Hide());
        heroes.ForEach(x => x.Hide());
    } 
}
