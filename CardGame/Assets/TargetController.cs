﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TargetController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] Image target;
    [SerializeField] List<CharacterView> characters;
    [SerializeField] public List<CharController> charControllers;
    public bool targetable;
    RectTransform rectTransform;
    TargetingController targetingController;

    private void Start()
    {
        Hide();
        rectTransform = GetComponent<RectTransform>();
        targetingController = FindObjectOfType<TargetingController>();
    }

    public void Show()
    {
        target.enabled = true;
        characters.ForEach(x => x.Show());
    }

    public void Hide()
    {
        target.enabled = false;
        characters.ForEach(x => x.Hide());
    }

    public void SetFocus(bool focus)
    {
        characters.ForEach(x => x.SetFocus(focus));
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SetFocus(true);
        targetingController.SetTarget(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        SetFocus(false);
        targetingController.RemoveTarget(this);
    }

    private void Update()
    {
        
    }
}
